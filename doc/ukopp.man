.TH UKOPP 1 2017-01-08 "Linux" "Ukopp man page"
 
.SH NAME
   Ukopp - copy files to USB stick or other disk or disk-like device
 
.SH SYNOPSIS
   \fBukopp\fR [ \fB-nogui\fR ] [ \fB-job\fR | \fB-run\fR ] \fIjobfile\fR
 
.SH DESCRIPTION
   Ukopp is a graphical menu-driven file backup utility operating in its 
   own window. Ukopp copies the files and directories specified in the 
   job file to a disk or disk-like media (e.g. USB stick). 
 
.SH OVERVIEW
   Ukopp works incrementally: only files that are not already identical 
   on the archive media are copied (new and modified files or files newly
   added to the backup job). Files/directories to include or exclude can 
   be selected at each level the file system hierarchy, using a graphic 
   navigator. Specifications are saved in a job file which can be edited 
   and executed. Ukopp can optionally keep old file versions instead of 
   replacing them with updated versions. The number of file versions to 
   keep and their retention time can be specified at each selection level 
   in the file hierarchy. File owners and permissions are retained even if 
   the archive media has a Microsoft file system.
   
   Summary of functionality:
    + Copy files incrementally with optional file version retention.
    + Three media verification modes: full, incremental, compare.
    + Report the differences between current and archive files.
    + Select and restore files from archive media or use drag and drop.
    + Compare current and archive file versions, differences list or
      side-by-side view.
 
.SH OPTIONS
   Command line options:
   [ \fB-job\fR ] \fIjobfile\fR           open job file for editing
   [ \fB-nogui\fR ] \fB-run\fR \fIjobfile\fR    execute a job file
 
.SH SEE ALSO
   The online user manual is available using the menu Help > contents.
   This manual explains Ukopp operation in great detail.
   
.SH AUTHORS
   Written by Michael Cornelison <kornelix@posteo.de>
   http://kornelix.net


