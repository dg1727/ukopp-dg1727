/********************************************************************************
   ukopp - disk to disk backup and restore program

   Copyright 2007-2017 Michael Cornelison  
   source URL:  kornelix.net
   contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************************/

#include <dirent.h>
#include <fcntl.h>
#include "zfuncs.h"

#define ukopp_title "ukopp v.6.2"                                                //  version
#define ukopp_license "GNU General Public License v.3"

//  parameters and limits

#define BIOCC  512*1024                                                          //  read and write I/O buffer size
#define maxnx 200                                                                //  max include/exclude in job file
#define maxfs 1000000                                                            //  max disk files, 1 million
#define MODTIMETOLR 2.0                                                          //  tolerance for "equal" mod times
#define nano 0.000000001                                                         //  nanosecond
#define mega (1024*1024)                                                         //  computer million
#define VSEP1 " ("                                                               //  file version appendage format:
#define VSEP2 ")"                                                                //     /.../filename (nnn)
#define RSEP1 " ("                                                               //  file retention appendage format:
#define RSEP2 ")"                                                                //     /.../filename (nn,nn)

//  special control files in archive directory

#define BD_UKOPPDIRK  "/ukopp-data"                                              //  directory for special files
#define BD_POOPFILE   "/ukopp-data/poopfile"                                     //  file owner & permissions file
#define BD_JOBFILE    "/ukopp-data/jobfile"                                      //  archive job file
#define BD_DATETIME   "/ukopp-data/datetime"                                     //  archive date-time file

//  GTK GUI widgets

#define MWIN GTK_WINDOW(mWin)

GtkWidget      *mWin, *mVbox, *mScroll, *mLog;                                   //  main window
GtkWidget      *fc_widget;                                                       //  file-chooser dialog widget
GtkWidget      *editwidget;                                                      //  edit box in file selection dialogs
GdkCursor      *watchcursor;
GdkWindow      *mLogwin;

//  file scope variables

int      main_argc;                                                              //  command line args
char     **main_argv;

int      killFlag;                                                               //  tell function to quit
int      pauseFlag;                                                              //  tell function to pause/resume
int      menuLock;                                                               //  menu lock flag
int      clrun;                                                                  //  flag, command line 'run' command

char     TFbakfiles[100];                                                        //  /home/user/.ukopp/xxx temp. files
char     TFjobfile[100], TFpoopfile[100];
char     TFdatetime[100];

//  disk devices and mount points

char     diskdev[100][40];                                                       //  /dev/xxx
char     diskdesc[100][60];                                                      //  device description
char     diskmp[100][60];                                                        //  mount point, /media/xxxx
int      Ndisk, maxdisk = 99;                                                    //  max. disks / partitions
int      devMounted = 0;                                                         //  archive device mounted status
int      ukoppMounted = 0;                                                       //  device was mounted by ukopp

//  backup job data

char     BJfilespec[XFCC];                                                       //  backup job file
int      BJnnx;                                                                  //  filespec count, 0...maxnx
int      BJrtype[maxnx];                                                         //    1/2/3 = comment/include/exclude
char    *BJfspec[maxnx];                                                         //    filespec (wild)
int      BJretNV[maxnx];                                                         //    retention versions
int      BJretND[maxnx];                                                         //    retention days
int      BJfiles[maxnx];                                                         //    count of matching disk files
double   BJbytes[maxnx];                                                         //    matching files byte count
int      BJvmode;                                                                //  0/1/2/3 = none/incr/full/comp
int      BJcopyop;                                                               //  1 = copy file owner/permissions    5.3
char     BJdev[40] = "";                                                         //  archive device
char     BJmp[200] = "";                                                         //  device mount point
char     BJdirk[200] = "";                                                       //  archive directory
int      BJdcc;                                                                  //  archive directory cc
int      BJvalid = 0;                                                            //  backup job valid flag
int      BJedited = 0;                                                           //  job edited and not saved

const char  *vertype[4] = { "none","incremental","full","compare" };             //  verify types

//  disk files specified in backup job

struct dfrec {                                                                   //  disk file record
   char    *file;                                                                //    file: /directory.../filename
   double   size;                                                                //    byte count
   double   mtime;                                                               //    mod time
   int      err;                                                                 //    fstat() status
   int      jindx;                                                               //    index to job data BJfspec[] etc.
   int      bindx;                                                               //    index to archive files Brec[]
   int      finc;                                                                //    included in curr. archive
   char     disp;                                                                //    status: new mod unch
};

int      Dnf;                                                                    //  actual file count < maxfs
double   Dbytes;                                                                 //  disk files, total bytes
dfrec    Drec[maxfs];                                                            //  disk file data records

//  archive files (copies at archive location)

struct   bfrec {                                                                 //  archive file record
   char    *file;                                                                //    file: /directory.../filename
   double   size;                                                                //    byte count
   double   mtime;                                                               //    mod time
   int      err;                                                                 //    file fstat() status
   int      retNV;                                                               //    retention versions
   int      retND;                                                               //    retention days
   int      lover, hiver;                                                        //    range of previous versions
   int      nexpv;                                                               //    no. expired versions
   int      finc;                                                                //    included in curr. backup job
   char     disp;                                                                //    file status: del mod unch
};

int      Bnf;                                                                    //  actual file count < maxfs
double   Bbytes;                                                                 //  archive files, total bytes
bfrec    Brec[maxfs];                                                            //  archive file data records
                                                                                 //  archive file statistics:
int      Cfiles;                                                                 //    curr. version file count
double   Cbytes;                                                                 //       and total bytes
int      Vfiles;                                                                 //    prior version file count
double   Vbytes;                                                                 //       and total bytes
int      Pfiles;                                                                 //    expired prior versions
double   Pbytes;                                                                 //       and total bytes
int      fverrs, fcerrs;                                                         //  file verify and compare errors

//  disk::archive comparison data

int      nnew, ndel, nmod, nunc, nexpv;                                          //  new, del, mod, unc, expv file counts
int      Mfiles;                                                                 //  new + mod + del file count
double   Mbytes;                                                                 //  new + mod files, total bytes

//  restore job data

char     RJfrom[300];                                                            //  restore copy-from: /directory/.../
char     RJto[300];                                                              //  restore copy-to: /directory/.../
int      RJnnx;                                                                  //  filespec count, 0...maxnx
int      RJrtype[maxnx];                                                         //    record type: include/exclude
char    *RJfspec[maxnx];                                                         //    filespec (wild)
int      RJvalid;                                                                //  restore job valid flag

//  restore file data

struct   rfrec {                                                                 //  restore file record
   char     *file;                                                               //  restore filespec: /directory.../file
   int      finc;                                                                //  flag, file restore was done
};

rfrec    Rrec[maxfs];                                                            //  restore file data records
int      Rnf;                                                                    //  actual file count < maxfs

//  ukopp functions

int initfunc(void *data);                                                        //  GTK init function
void buttonfunc(GtkWidget *, cchar *menu);                                       //  process toolbar button event
void menufunc(GtkWidget *, cchar *menu);                                         //  process menu select event

int getroot(cchar *);                                                            //  get root privileges
int quit_ukopp(cchar *);                                                         //  exit application
int clearScreen(cchar *);                                                        //  clear logging window
int signalFunc(cchar *);                                                         //  kill/pause/resume curr. function
int checkKillPause();                                                            //  test flags: killFlag and pauseFlag

int DevPoop();                                                                   //  get all devices and mount points

int BJfileOpen(cchar *);                                                         //  job file open dialog
int BJfileSave(cchar *);                                                         //  job file save dialog
int BJload(cchar *fspec);                                                        //  backup job data <<< file
int BJstore(cchar *fspec);                                                       //  backup job data >>> file
int BJlist(cchar *);                                                             //  backup job >>> log window
int BJedit(cchar *);                                                             //  backup job edit dialog

cchar * parse_archive_rec(const char *text);                                     //  parse job file archive record
cchar * validate_archive();                                                      //  validate archive location
cchar * parseNXrec(cchar *, int &, char *&, int &, int &);                       //  parse include/exclude record for backup
cchar * parseRXrec(cchar *, int &, char *&);                                     //  parse include/exclude record for restore
cchar * parseVerify(cchar *);                                                    //  parse verify record

int Backup(cchar *);                                                             //  backup function
int Synch(cchar *);                                                              //  synchronize function
int Verify(cchar *);                                                             //  verify functions
int Report(cchar *);                                                             //  report functions
int FileDiffs(cchar *);                                                          //  compare file versions

int RJedit(cchar *);                                                             //  restore job edit dialog
int RJlist(cchar *);                                                             //  list archive files to be restored
int Restore(cchar *);                                                            //  file restore function

int helpFunc(cchar *);                                                           //  help function

void format_seconds(double secs, char *text);                                    //  convert seconds to "hh:mm:ss" 
int mount(cchar *);                                                              //  mount archive device
int unmount(cchar *);                                                            //  unmount archive device
int saveLog(cchar *);                                                            //  save log window to file
int writeDT();                                                                   //  write date-time to temp file
int synch_poop(const char *mode);                                                //  synch owner and permissions data

int dGetFiles();                                                                 //  generate backup files from job data
int bGetFiles();                                                                 //  get backup file list
int rGetFiles();                                                                 //  generate file list from restore job
int setFileDisps();                                                              //  set file disps: new del mod unch
int SortFileList(char *recs, int RL, int NR, char sort);                         //  sort file list in memory
int filecomp(cchar *file1, cchar *file2);                                        //  compare files, directories first

int BJreset();                                                                   //  reset backup job file data
int RJreset();                                                                   //  reset restore job data
int dFilesReset();                                                               //  reset disk file data and free memory
int bFilesReset();                                                               //  reset backup file data, free memory
int rFilesReset();                                                               //  reset restore file data, free memory

cchar * copyFile(cchar *file1, cchar *file2, int mpf, int copf);                 //  copy disk file << >> archive file
cchar * checkFile(cchar *file, int compf, double &bcc);                          //  validate file and return length
cchar * setnextVersion(bfrec &rec);                                              //  archive file: assign next version
cchar * purgeVersions(bfrec &rec, int fkeep);                                    //  archive file: delete expired vers.
cchar * deleteFile(cchar *file);                                                 //  delete archive file
int setFileVersion(char *file, int vers);                                        //  (re)set filespec version in memory
int do_shell(cchar *pname, cchar *command);                                      //  do shell command and echo outputs

//  ukopp menu table

struct menuent {
   char     menu1[20], menu2[40];                                                //  top-menu, sub-menu
   int      lock;                                                                //  lock funcs: no run parallel
   int      (*mfunc)(cchar *);                                                   //  processing function
};

#define nmenu  39
struct menuent menus[nmenu] = {
//  top-menu    sub-menu               lock    menu-function
{  "button",   "root",                   1,    getroot        },
{  "button",   "edit job",               1,    BJedit         },
{  "button",   "clear",                  0,    clearScreen    },
{  "button",   "run job",                1,    Backup         },
{  "button",   "mount",                  1,    mount          },
{  "button",   "unmount",                1,    unmount        },
{  "button",   "pause",                  0,    signalFunc     },
{  "button",   "resume",                 0,    signalFunc     },
{  "button",   "kill job",               0,    signalFunc     },
{  "button",   "quit",                   0,    quit_ukopp     },
{  "File",     "open job",               1,    BJfileOpen     },
{  "File",     "edit job",               1,    BJedit         },
{  "File",     "list job",               0,    BJlist         },
{  "File",     "save job",               0,    BJfileSave     },
{  "File",     "save job as",            0,    BJfileSave     },
{  "File",     "run job",                1,    Backup         },
{  "File",     "quit",                   0,    quit_ukopp     },
{  "Backup",   "backup only",            1,    Backup         },
{  "Backup",   "synchronize",            1,    Synch          },
{  "Verify",   "incremental",            1,    Verify         },
{  "Verify",   "full",                   1,    Verify         },
{  "Verify",   "compare",                1,    Verify         },
{  "Report",   "get disk files",         1,    Report         },
{  "Report",   "archived versions",      1,    Report         },
{  "Report",   "expired versions",       1,    Report         },
{  "Report",   "list disk files",        1,    Report         },
{  "Report",   "list archive files",     1,    Report         },
{  "Report",   "find files",             1,    Report         },
{  "Report",   "save window",            0,    saveLog        },
{  "Diffs",    "diffs summary",          1,    Report         },
{  "Diffs",    "diffs by directory",     1,    Report         },
{  "Diffs",    "diffs by file status",   1,    Report         },
{  "Diffs",    "diffs by file",          1,    Report         },
{  "Diffs",    "compare file versions",  1,    FileDiffs      },                 //  6.0
{  "Restore",  "setup restore job",      1,    RJedit         },
{  "Restore",  "list restore files",     1,    RJlist         },
{  "Restore",  "restore files",          1,    Restore        },
{  "Help",     "about",                  0,    helpFunc       },
{  "Help",     "user guide",             0,    helpFunc       }  }; 


/********************************************************************************/

//  ukopp main program

int main(int argc, char *argv[])
{
   GtkWidget   *mbar, *tbar;
   GtkWidget   *mFile, *mBackup, *mVerify, *mReport, *mRestore, *mDiffs;
   GtkWidget   *mHelp;
   GdkWindow   *gdkwin;
   GdkDisplay  *display;
   int         ii;

   gtk_init(&argc, &argv);                                                       //  GTK command line options

   zinitapp("ukopp");                                                            //  setup app directories

   clrun = 0;                                                                    //  no command line run command
   *BJfilespec = 0;                                                              //  no backup job file

   main_argc = argc;                                                             //  save command line arguments
   main_argv = argv;

   for (ii = 1; ii < argc; ii++)                                                 //  process command line
   {
      if (strmatch(argv[ii],"-job") && argc > ii+1)                              //  -job jobfile  (load only)
            strcpy(BJfilespec,argv[++ii]);
      else if (strmatch(argv[ii],"-run") && argc > ii+1)                         //  -run jobfile  (load and run)
          { strcpy(BJfilespec,argv[++ii]); clrun++; }
      else  strcpy(BJfilespec,argv[ii]);                                         //  assume a job file and load it
   }

   mWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);                                   //  create main window
   gtk_window_set_title(GTK_WINDOW(mWin),ukopp_title);
   gtk_window_set_position(GTK_WINDOW(mWin),GTK_WIN_POS_CENTER);
   gtk_window_set_default_size(GTK_WINDOW(mWin),800,500);

   mVbox = gtk_box_new(VERTICAL,0);                                              //  vertical packing box
   gtk_container_add(GTK_CONTAINER(mWin),mVbox);                                 //  add to main window

   mbar = create_menubar(mVbox);                                                 //  create menu bar

   mFile = add_menubar_item(mbar,"File",menufunc);                               //  add menu bar items
      add_submenu_item(mFile,"open job",menufunc);
      add_submenu_item(mFile,"edit job",menufunc);
      add_submenu_item(mFile,"list job",menufunc);
      add_submenu_item(mFile,"save job",menufunc);
      add_submenu_item(mFile,"save job as",menufunc);
      add_submenu_item(mFile,"run job",menufunc);
      add_submenu_item(mFile,"quit",menufunc);
   mBackup = add_menubar_item(mbar,"Backup",menufunc);
      add_submenu_item(mBackup,"backup only",menufunc);
      add_submenu_item(mBackup,"synchronize",menufunc);
   mVerify = add_menubar_item(mbar,"Verify",menufunc);
      add_submenu_item(mVerify,"incremental",menufunc);
      add_submenu_item(mVerify,"full",menufunc);
      add_submenu_item(mVerify,"compare",menufunc);
   mReport = add_menubar_item(mbar,"Report",menufunc);
      add_submenu_item(mReport,"get disk files",menufunc);
      add_submenu_item(mReport,"archived versions",menufunc);
      add_submenu_item(mReport,"expired versions",menufunc);
      add_submenu_item(mReport,"list disk files",menufunc);
      add_submenu_item(mReport,"list archive files",menufunc);
      add_submenu_item(mReport,"find files",menufunc);
      add_submenu_item(mReport,"save screen",menufunc);
   mRestore = add_menubar_item(mbar,"Restore",menufunc);
      add_submenu_item(mRestore,"setup restore job",menufunc);
      add_submenu_item(mRestore,"list restore files",menufunc);
      add_submenu_item(mRestore,"restore files",menufunc);
   mDiffs = add_menubar_item(mbar,"Diffs",menufunc);
      add_submenu_item(mDiffs,"diffs summary",menufunc);
      add_submenu_item(mDiffs,"diffs by directory",menufunc);
      add_submenu_item(mDiffs,"diffs by file status",menufunc);
      add_submenu_item(mDiffs,"diffs by file",menufunc);
      add_submenu_item(mDiffs,"compare file versions",menufunc);
   mHelp = add_menubar_item(mbar,"Help",menufunc);
      add_submenu_item(mHelp,"about",menufunc);
      add_submenu_item(mHelp,"user guide",menufunc);

   tbar = create_toolbar(mVbox,32);                                              //  create toolbar and buttons
   gtk_toolbar_set_style(GTK_TOOLBAR(tbar),GTK_TOOLBAR_BOTH);

   if (getuid() > 0)
      add_toolbar_button(tbar,"root","get root privileges","root.png",buttonfunc);
   else
      add_toolbar_button(tbar,"root","you have root privileges","root.png",buttonfunc);

   add_toolbar_button(tbar,"mount","mount archive device","mount.png",buttonfunc);
   add_toolbar_button(tbar,"unmount","unmount archive device","unmount.png",buttonfunc);
   add_toolbar_button(tbar,"edit job","edit backup job","edit.png",buttonfunc);
   add_toolbar_button(tbar,"run job","run backup job","run.png",buttonfunc);
   add_toolbar_button(tbar,"pause","pause running job","media-pause.png",buttonfunc);
   add_toolbar_button(tbar,"resume","resume running job","media-play.png",buttonfunc);
   add_toolbar_button(tbar,"kill job","kill running job","stop.png",buttonfunc);
   add_toolbar_button(tbar,"clear","clear screen","clear.png",buttonfunc);
   add_toolbar_button(tbar,"quit","quit ukopp","quit.png",buttonfunc);

   mScroll = gtk_scrolled_window_new(0,0);                                       //  scrolled window
   gtk_box_pack_end(GTK_BOX(mVbox),mScroll,1,1,0);                               //  add to main window mVbox

   mLog = gtk_text_view_new();                                                   //  text edit window
   gtk_text_view_set_left_margin(GTK_TEXT_VIEW(mLog),2);
   gtk_container_add(GTK_CONTAINER(mScroll),mLog);                               //  add to scrolled window

   gtk_widget_show_all(mWin);                                                    //  show all widgets
   get_hardware_info();                                                          //  v.5.9

   G_SIGNAL(mWin,"destroy",quit_ukopp,0);                                        //  connect window destroy event
   G_SIGNAL(mWin,"delete_event",quit_ukopp,0);

   gdkwin = gtk_widget_get_window(mWin);
   display = gdk_window_get_display(gdkwin);
   watchcursor = gdk_cursor_new_for_display(display,GDK_WATCH);

   mLogwin = gtk_text_view_get_window(GTK_TEXT_VIEW(mLog),                       //  GDK window for mLog
                                       GTK_TEXT_WINDOW_TEXT);
   zdialog_inputs("load");                                                       //  load saved dialog inputs           6.0

   g_timeout_add(0,initfunc,0);                                                  //  setup initial call from gtk_main()
   gtk_main();                                                                   //  process window events
   return 0;
}


//  initial function called from gtk_main() at startup

int initfunc(void *data)
{
   int         ii;
   const char  *home, *appdirk;
   time_t      datetime;

   datetime = time(0);
   wprintff(mLog,"ukopp %s \n",ctime(&datetime));

   if (getuid() == 0)
      wprintx(mLog,0,"you have root privileges \n",1);
   else {
      menufunc(null,"Help");                                                     //  show version and license
      menufunc(null,"about");
   }

   appdirk = get_zhomedir();
   sprintf(TFbakfiles,"%s/bakfiles",appdirk);                                    //  make temp file names
   sprintf(TFpoopfile,"%s/poopfile",appdirk);
   sprintf(TFjobfile,"%s/jobfile",appdirk);
   sprintf(TFdatetime,"%s/datetime",appdirk);

   menuLock = killFlag = pauseFlag = 0;                                          //  initialize controls

   BJnnx = 4;                                                                    //  default backup job data
   for (ii = 0; ii < BJnnx; ii++)
      BJfspec[ii] = (char *) zmalloc(60);
   home = getenv("HOME");                                                        //  get "/home/username"
   if (! home) home = "/root";

   strcpy(BJfspec[0],"# default backup job");                                    //  comment
   sprintf(BJfspec[1],"%s/*",home);                                              //  /home/username/*
   sprintf(BJfspec[2],"%s/*/Trash/*",home);                                      //  /home/username/*/Trash/*
   sprintf(BJfspec[3],"%s/.thumbnails/*",home);                                  //  /home/username/.thumbnails/*

   BJrtype[0] = 1;                                                               //  comment
   BJrtype[1] = 2;                                                               //  include
   BJrtype[2] = 3;                                                               //  exclude
   BJrtype[3] = 3;                                                               //  exclude
   
   BJcopyop = 1;                                                                 //  copy owner/permissions             5.3

   BJretNV[1] = BJretND[1] = 0;                                                  //  no retention specs
   BJvmode = 0;                                                                  //  no verify
   BJvalid = 0;                                                                  //  not validated
   
   *BJdev = *BJmp = *BJdirk = BJdcc = 0;                                         //  no archive device, mount point, directory

   strcpy(RJfrom,"/home/");                                                      //  file restore copy-from location
   strcpy(RJto,"/home/");                                                        //  file restore copy-to location
   RJnnx = 0;                                                                    //  no. restore include/exclude recs
   RJvalid = 0;                                                                  //  not validated

   DevPoop();                                                                    //  find devices and mount points

   if (*BJfilespec) BJload(BJfilespec);                                          //  load command line job file
   else snprintf(BJfilespec,XFCC,"%s/ukopp.job",get_zhomedir());                 //  or set default job file

   if (clrun) {
      menufunc(null,"File");                                                     //  run command line job file
      menufunc(null,"run job");
   }

   return 0;
}


//  process toolbar button events (simulate menu selection)

void buttonfunc(GtkWidget *, cchar *button)
{
   char     button2[20], *pp;

   strncpy0(button2,button,19);
   pp = strchr(button2,'\n');                                                    //  replace \n with blank
   if (pp) *pp = ' ';

   menufunc(0,"button");
   menufunc(0,button2);
   return;
}


//  process menu selection event

void menufunc(GtkWidget *, cchar *menu)                                          //  revised for change in GTK behavior
{                                                                                //    starting with Ubuntu 14.04
   int            ii;
   char           command[100];

   for (ii = 0; ii < nmenu; ii++)                                                //  search menu table
      if (strmatch(menu,menus[ii].menu2)) break;                                 //  mark sub-menu selection
   if (ii == nmenu) return;                                                      //  a top menu, ignore

   if (menuLock && menus[ii].lock) {                                             //  no lock funcs can run parallel
      zmessageACK(mWin,"a blocking function is active");
      return;
   }

   if (! menuLock)
      killFlag = pauseFlag = 0;                                                  //  reset controls

   snprintf(command,99,"\n""command: %s \n",menu);
   wprintx(mLog,0,command,1);

   if (menus[ii].lock) ++menuLock;
   menus[ii].mfunc(menu);                                                        //  call menu function
   if (menus[ii].lock) --menuLock;

   return;
}


//  get root privileges if password is OK

int getroot(cchar * menu)
{
   if (getuid() == 0)
      wprintx(mLog,0,"\nyou have root privileges \n",1);
   else
      beroot(main_argc-1,main_argv+1);                                           //  does not return
   return 0;
}


//  quit ukopp

int quit_ukopp(cchar *menu)
{
   int      yn;
   char     logfile[200];

   if (devMounted && ukoppMounted) unmount(0);

   if (BJedited) {
      yn = zmessageYN(mWin,"job file modified, QUIT anyway?");
      if (! yn) return 1;
      BJedited = 0;
   }

   if (mLog) {
      sprintf(logfile,"%s/ukopp.log2",get_zhomedir());                           //  dump window to log file
      wfiledump(mLog,logfile);
   }

   zdialog_inputs("save");                                                       //  save dialog inputs                 6.0
   gtk_main_quit();                                                              //  tell gtk_main() to quit
   return 0;
}


//  clear logging window

int clearScreen(cchar *menu)
{
   wclear(mLog);
   return 0;
}


//  kill/pause/resume current function - called from menu function

int signalFunc(cchar *menu)
{
   if (strmatch(menu,"kill job"))
   {
      if (! menuLock) {
         wprintff(mLog,"\n"" ready \n");
         return 0;
      }

      if (killFlag) {
         wprintff(mLog," *** waiting for function to quit \n");
         return 0;
      }

      wprintff(mLog," *** KILL current function \n");
      pauseFlag = 0;
      killFlag = 1;
      return 0;
   }

   if (strmatch(menu,"pause")) {
      pauseFlag = 1;
      return 0;
   }

   if (strmatch(menu,"resume")) {
      pauseFlag = 0;
      return 0;
   }

   else zappcrash("signalFunc: %s",menu);
   return 0;
}


//  check kill and pause flags
//  called periodically from long-running functions

int checkKillPause()
{
   while (pauseFlag)                                                             //  idle loop while paused
   {
      zsleep(0.1);
      zmainloop();                                                               //  process menus
   }

   zmainloop();                                                                  //  keep menus working

   if (! killFlag) return 0;                                                     //  keep running
   return 1;                                                                     //  die now and reset killFlag
}


/********************************************************************************/

//  find all disk devices and mount points via Linux utilities

int DevPoop()                                                                    //  new udevinfo format
{
   int      ii, jj, contx = 0, pii, pjj;
   int      diskf, filsysf, usbf, Nth, Nmounted;
   char     *buff, diskdev1[40], diskdesc1[60], work[300];
   cchar    *pp1, *pp2;

   Ndisk = diskf = filsysf = usbf = 0;

   while ((buff = command_output(contx,"udevadm info -e")))
   {
      if (strmatchN(buff,"P: ",3)) {                                             //  start new device
         if (diskf && filsysf) {                                                 //  if last device = formatted disk
            strncpy0(diskdev[Ndisk],diskdev1,39);                                //    save /dev/devid
            strncpy0(diskdesc[Ndisk],diskdesc1,59);                              //    save description
            if (usbf) strcat(diskdesc[Ndisk]," (USB)");                          //    note if USB device
            strcpy(diskmp[Ndisk],"(not mounted)");                               //    mount point TBD
            Ndisk++;
            if (Ndisk == maxdisk) {
               wprintff(mLog," *** exceeded %d devices \n",maxdisk);
               break;
            }
         }

         diskf = filsysf = usbf = 0;                                             //  clear new device flags
      }

      if (strmatchN(buff,"N: ",3)) {
         strcpy(diskdev1,"/dev/");
         strncat(diskdev1,buff+3,14);                                            //  save /dev/devid
      }

      if (strmatchN(buff,"E: ",3)) {
         pp1 = strstr(buff,"ID_TYPE=disk");
         if (pp1) diskf = 1;                                                     //  device is a disk
         pp1 = strstr(buff,"ID_FS_TYPE=");
         if (pp1) filsysf = 1;                                                   //  device has a file system
         pp1 = strstr(buff,"ID_BUS=usb");
         if (pp1) usbf = 1;                                                      //  device is a USB device
         pp1 = strstr(buff,"ID_MODEL=");
         if (pp1) strncpy0(diskdesc1,pp1+9,59);                                  //  save description
      }
   }

   if (! Ndisk) {
      wprintff(mLog," no devices found \n");
      return 0;
   }

   contx = Nmounted = 0;

   while ((buff = command_output(contx,"cat /proc/mounts")))                     //  get mounted disk info
   {
      if (! strmatchN(buff,"/dev/",5)) continue;                                 //  not a /dev/xxx record

      Nth = 1;
      pp1 = strField(buff,' ',Nth++);                                            //  parse /dev/xxx /media/xxx
      pp2 = strField(buff,' ',Nth++);

      for (ii = 0; ii < Ndisk; ii++)                                             //  look for matching device
      {
         if (! strmatch(pp1,diskdev[ii])) continue;
         strncpy0(diskmp[ii],pp2,59);                                            //  copy its mount point
         strTrim(diskmp[ii]);
         Nmounted++;
         break;
      }
   }

   #define swap(name,ii,jj) {             \
         strcpy(work,name[ii]);           \
         strcpy(name[ii],name[jj]);       \
         strcpy(name[jj],work); }

   for (ii = 0; ii < Ndisk; ii++)                                                //  sort USB and mounted devices
   for (jj = ii + 1; jj < Ndisk; jj++)                                           //    to the top of the list
   {
      pii = pjj = 0;
      if (strstr(diskdesc[ii],"(USB)")) pii += 2;
      if (! strmatch(diskmp[ii],"(not mounted)")) pii += 1;
      if (strstr(diskdesc[jj],"(USB)")) pjj += 2;
      if (! strmatch(diskmp[jj],"(not mounted)")) pjj += 1;
      if (pjj > pii) {
         swap(diskdev,jj,ii);
         swap(diskmp,jj,ii);
         swap(diskdesc,jj,ii);
      }
   }

   return Nmounted;
}


/********************************************************************************/

//  job file open dialog - get backup job data from a file
//  return 1 if OK, else 0

int BJfileOpen(cchar *menu)
{
   char        *file;

   file = zgetfile("open backup job",MWIN,"file",BJfilespec,1);                  //  get file from user
   if (file) {
      strncpy0(BJfilespec,file,XFCC-2);
      zfree(file);
      BJload(BJfilespec);                                                        //  load job file, set BJvalid
   }

   return 0;
}


//  job file save dialog - save backup job data to a file
//  return 1 if OK, else 0

int BJfileSave(cchar *menu)
{
   char        *file;
   int         yn;

   if (! BJvalid) {
      yn = zmessageYN(mWin,"backup job has errors, save anyway?");               //  6.0
      if (! yn) return 0;
   }

   if (strmatch(menu,"save job")) {
      BJstore(BJfilespec);
      return 0;
   }

   file = zgetfile("save backup job",MWIN,"save",BJfilespec,1);
   if (file) {
      strncpy0(BJfilespec,file,XFCC-2);
      zfree(file);
      BJstore(BJfilespec);
   }

   return 0;
}


//  backup job data <<< jobfile

int BJload(cchar *jobfile)
{
   FILE           *fid;
   char           *pp, *fspec, buff[1000];
   const char     *errmess, *jobname, *pcop;
   int            rtype, vers, days, nerrs = 0;
   
   BJvalid = 0;

   if (! jobfile || *jobfile != '/') {                                           //  6.0
      wprintff(mLog," *** no job file specified \n");
      return 0;
   }

   snprintf(buff,999,"\n""loading job file: %s \n",jobfile);
   wprintx(mLog,0,buff,1);

   fid = fopen(jobfile,"r");                                                     //  open job file
   if (! fid) {
      wprintff(mLog," *** cannot open job file: %s \n",jobfile);
      return 0;
   }

   BJreset();                                                                    //  reset all job data

   while (true)
   {
      pp = fgets_trim(buff,999,fid,1);                                           //  read next job record
      if (! pp) break;                                                           //  EOF

      wprintff(mLog," %s \n",buff);                                              //  output

      if (strmatchN(pp,"archive",7)) {
         errmess = parse_archive_rec(buff);                                      //  archive /dev/xxx /mountpoint /directory
         if (errmess) {
            wprintff(mLog," *** %s \n",errmess);
            nerrs++;                                                             //  bugfix                             5.6
            continue;
         }
         errmess = validate_archive();
         if (errmess) {
            wprintff(mLog," %s \n",errmess);
            nerrs++;                                                             //  bugfix                             5.6
         }
         continue;
      }

      if (strmatchN(pp,"verify",6)) {
         errmess = parseVerify(buff);                                            //  verify xxxxxx
         if (errmess) wprintff(mLog," *** %s \n",errmess);
         if (errmess) nerrs++;
         continue;
      }
      
      if (strmatchN(pp,"copy_owner_permissions",22)) {                           //  copy_owner_permissions Y/N         5.3
         pcop = strField(pp,' ',2);
         if (! pcop) {
            wprintff(mLog," *** copy_owner_permissions not Y/N \n");
            nerrs++;
            continue;
         }
         if (*pcop == 'Y' || *pcop == 'y') BJcopyop = 1;
         else if (*pcop == 'N' || *pcop == 'n') BJcopyop= 0;
         else {
            wprintff(mLog," *** copy_owner_permissions not Y/N \n");
            nerrs++;
            continue;
         }
         continue;
      }

      errmess = parseNXrec(buff,rtype,fspec,vers,days);                          //  comment/include/exclude
      if (errmess) wprintff(mLog," *** %s \n",errmess);
      if (errmess) nerrs++;
      BJfspec[BJnnx] = fspec;
      BJrtype[BJnnx] = rtype;
      BJretNV[BJnnx] = vers;
      BJretND[BJnnx] = days;
      BJnnx++;

      if (BJnnx == maxnx) {
         wprintff(mLog," *** max job records exceeded \n");
         nerrs++;
         break;
      }
   }

   fclose(fid);                                                                  //  close file

   if (nerrs == 0) {
      BJvalid = 1;                                                               //  job valid if no errors
      jobname = strrchr(BJfilespec,'/');
      if (! jobname) jobname = BJfilespec;                                       //  bugfix                             5.2
      else jobname++;
      snprintf(buff,100,"%s   %s",ukopp_title,jobname);                          //  put job name in window title
      gtk_window_set_title(GTK_WINDOW(mWin),buff);
      BJedited = 0;
   }

   return 1;
}


//  backup job data >>> jobfile
//  return 1 if OK, else 0

int BJstore(cchar *jobfile)
{
   FILE     *fid;
   char     buff[100];
   cchar    *jobname;

   fid = fopen(jobfile,"w");                                                     //  open file
   if (! fid) {
      wprintff(mLog," *** cannot open job file: %s \n",jobfile);
      return 0;
   }

   if (*BJdev)    
      fprintf(fid,"archive %s %s %s \n",BJdev,BJmp,BJdirk);                      //  archive /dev/sdb2 /mount /mount/directory
   else fprintf(fid,"archive (none) (none) %s\n",BJdirk);                        //  archive (none) (none) /directory

   fprintf(fid,"verify %s \n",vertype[BJvmode]);                                 //  verify xxxx

   if (BJcopyop) fprintf(fid,"copy_owner_permissions Y \n");                     //  copy_owner_permissions Y/N         5.3
   else fprintf(fid,"copy_owner_permissions N \n");   

   for (int ii = 0; ii < BJnnx; ii++)
   {
      if (BJrtype[ii] == 1)
         fprintf(fid,"%s \n",BJfspec[ii]);                                       //  comment

      if (BJrtype[ii] == 2) {
         if (BJretNV[ii] + BJretND[ii] > 0)                                      //  include /filespec (nv,nd)
            fprintf(fid,"include %s%s%d,%d%s\n",
                     BJfspec[ii],RSEP1,BJretNV[ii],BJretND[ii],RSEP2);
         else  fprintf(fid,"include %s\n",BJfspec[ii]);
      }

      if (BJrtype[ii] == 3)                                                      //  exclude /filespec
         fprintf(fid,"exclude %s\n",BJfspec[ii]);
   }

   fclose(fid);

   if (! strmatch(jobfile,TFjobfile)) {                                          //  if not job file in temp storage,
      jobname = strrchr(jobfile,'/') + 1;
      snprintf(buff,100,"%s   %s",ukopp_title,jobname);                          //  put job name in window title
      gtk_window_set_title(GTK_WINDOW(mWin),buff);
      BJedited = 0;
   }

   return 1;
}


//  list backup job data to log window

int BJlist(cchar *menu)
{
   wprintff(mLog,"\n backup job file: %s \n",BJfilespec);                        //  job file
   
   if (*BJdev)
      wprintff(mLog," archive device: %s  mount point: %s \n",BJdev,BJmp);       //  /dev/sdxx /xxxx
   else 
      wprintff(mLog," archive device: (none)  mount point: (none) \n");

   wprintff(mLog," archive directory: %s \n",BJdirk);                            //  /xxxx/yyyy

   wprintff(mLog," verify: %s \n",vertype[BJvmode]);                             //  verify: xxxx

   if (BJcopyop) wprintff(mLog," copy_owner_permissions: Y \n");                 //  copy_owner_permissions Y/N         5.3
   else wprintff(mLog," copy_owner_permissions: N \n");

   for (int ii = 0; ii < BJnnx; ii++)
   {
      if (BJrtype[ii] == 1)                                                      //  comment
         wprintff(mLog," %s \n",BJfspec[ii]);

      if (BJrtype[ii] == 2) {                                                    //  include /filespec (nv,nd)
         if (BJretNV[ii] + BJretND[ii] > 0)
            wprintff(mLog," include %s%s%d vers, %d days%s \n",
                     BJfspec[ii],RSEP1,BJretNV[ii],BJretND[ii],RSEP2);
         else  wprintff(mLog," include %s \n",BJfspec[ii]);
      }

      if (BJrtype[ii] == 3)                                                      //  exclude /filespec
         wprintff(mLog," exclude %s \n",BJfspec[ii]);
   }

   return 0;
}


/********************************************************************************/

//  edit dialog for backup job data

int  BJedit_fchooser(cchar *dirk);
zdialog *BJedit_fchooser_zd = 0;

char     orgBJdev[40];
char     orgBJmp[200];
char     orgBJdirk[200];

int BJedit(cchar *menu)
{
   int BJedit_dialog_event(zdialog *zd, const char *event);

   zdialog     *zd;

/****
       __________________________________________________________________
      |                     Edit Backup Job                              |
      |                                                                  |
      | archive device: /dev/sdxx                          [choose]      |
      | mount point: /xxxxx/yyyyyyyy                                     |
      | directory: /xxxxx/yyyyyyyy/zzzzzzzz                              |
      |                                                                  |
      | verify method: (o) none  (o) incremental  (o) full  (o) compare  |
      | [x] copy owner/permissions                                       |
      |                                                                  |
      |  ___________________Include / Exclude__________________________  |
      | |                                                              | |
      | | include /home/johndoe/...                                    | |
      | | exclude /home/johndoe/...                                    | |
      | | ...                                                          | |
      | |                                                              | |
      | |                                                              | |
      | |______________________________________________________________| |
      |                                                                  |
      |                                [browse] [clear] [done] [cancel]  |
      |__________________________________________________________________|

****/

   zd = zdialog_new("Edit Backup Job",mWin,"browse","clear","done","cancel",null);
   
   zdialog_add_widget(zd,"hbox","hbdev","dialog");                               //  archive device: /dev/sdxx     [choose]
   zdialog_add_widget(zd,"label","labdev","hbdev","archive device:");            //  v.5.6
   zdialog_add_widget(zd,"label","BJdev","hbdev","/dev/sdxx","space=3");

   zdialog_add_widget(zd,"hbox","hbmp","dialog");                                //  mount point: /xxxxx
   zdialog_add_widget(zd,"label","labmp","hbmp","mount point:");
   zdialog_add_widget(zd,"label","BJmp","hbmp","/xxxxxxx","space=3");

   zdialog_add_widget(zd,"hbox","hbdir","dialog");                               //  directory: /xxxxx/yyyyyy
   zdialog_add_widget(zd,"label","labdir","hbdir","directory:");
   zdialog_add_widget(zd,"label","BJdirk","hbdir","/xxxxxxx/yyyyyyy","space=3");

   zdialog_add_widget(zd,"hbox","hbchoose","dialog");   
   zdialog_add_widget(zd,"label","labchoose","hbchoose","choose device, mount point, directory:");
   zdialog_add_widget(zd,"button","choose","hbchoose","choose","space=5");

   zdialog_add_widget(zd,"hsep","sep1","dialog");
   zdialog_add_widget(zd,"hbox","hb2","dialog");                                 //  verify: (o) none (o) incr (o) ...
   zdialog_add_widget(zd,"label","labverify","hb2","verify method:");
   zdialog_add_widget(zd,"radio","vnone","hb2","none","space=3");
   zdialog_add_widget(zd,"radio","vincr","hb2","incremental","space=3");
   zdialog_add_widget(zd,"radio","vfull","hb2","full","space=3");
   zdialog_add_widget(zd,"radio","vcomp","hb2","compare","space=3");
   
   zdialog_add_widget(zd,"hbox","hb3","dialog");                                 //  [x] copy owner/permissions         5.3
   zdialog_add_widget(zd,"check","bjcop","hb3","copy owner/permissions");

   zdialog_add_widget(zd,"hsep","sep2","dialog");                                //  edit box for job recs
   zdialog_add_widget(zd,"label","labinex","dialog","Include / Exclude");
   zdialog_add_widget(zd,"frame","frminex","dialog",0,"expand");
   zdialog_add_widget(zd,"scrwin","scrwinex","frminex");
   zdialog_add_widget(zd,"edit","edinex","scrwinex");

   if (*BJdev) {
      zdialog_stuff(zd,"BJdev",BJdev);
      zdialog_stuff(zd,"BJmp",BJmp);
   }
   else {
      zdialog_stuff(zd,"BJdev","(none)");
      zdialog_stuff(zd,"BJmp","(none)");
   }
   zdialog_stuff(zd,"BJdirk",BJdirk);
   
   strncpy0(orgBJdev,BJdev,40);                                                  //  save in case of cancel
   strncpy0(orgBJmp,BJmp,200);
   strncpy0(orgBJdirk,BJdirk,200);

   zdialog_stuff(zd,"vnone",0);                                                  //  stuff verify mode
   zdialog_stuff(zd,"vincr",0);
   zdialog_stuff(zd,"vfull",0);
   zdialog_stuff(zd,"vcomp",0);
   if (BJvmode == 0) zdialog_stuff(zd,"vnone",1);
   if (BJvmode == 1) zdialog_stuff(zd,"vincr",1);
   if (BJvmode == 2) zdialog_stuff(zd,"vfull",1);
   if (BJvmode == 3) zdialog_stuff(zd,"vcomp",1);
   
   zdialog_stuff(zd,"bjcop",BJcopyop);                                           //  stuff copy owner/perm option       5.3

   editwidget = zdialog_widget(zd,"edinex");
   wclear(editwidget);                                                           //  stuff include/exclude recs

   for (int ii = 0; ii < BJnnx; ii++)
   {
      if (BJrtype[ii] == 1)                                                      //  comment
         wprintff(editwidget,"%s\n",BJfspec[ii]);

      if (BJrtype[ii] == 2) {                                                    //  include /filespec (nv,nd)
         if (BJretNV[ii] + BJretND[ii] > 0)
            wprintff(editwidget,"include %s%s%d,%d%s\n",
                        BJfspec[ii],RSEP1,BJretNV[ii],BJretND[ii],RSEP2);
         else  wprintff(editwidget,"include %s\n",BJfspec[ii]);
      }

      if (BJrtype[ii] == 3)                                                      //  exclude /filespec
         wprintff(editwidget,"exclude %s\n",BJfspec[ii]);
   }

   zdialog_resize(zd,400,400);
   zdialog_run(zd,BJedit_dialog_event,"40/10");                                  //  run dialog
   zdialog_wait(zd);                                                             //  wait for completion
   return 0;
}


//  job edit dialog event function

int BJedit_dialog_event(zdialog *zd, const char *event)
{
   void BJedit_chooseArchive();

   int         rtype, vers, days, nerrs = 0;
   char        *pp, *fspec, text[300];
   cchar       *errmess = 0, *jobname;
   int         zstat, nn, ftf = 1;

   if (strmatch(event,"choose")) {                                               //  choose new archive location        5.6
      BJedit_chooseArchive();
      if (*BJdev) {
         zdialog_stuff(zd,"BJdev",BJdev);
         zdialog_stuff(zd,"BJmp",BJmp);
         zdialog_stuff(zd,"BJdirk",BJdirk);
      }
      else {
         zdialog_stuff(zd,"BJdev","(none)");
         zdialog_stuff(zd,"BJmp","(none)");
         zdialog_stuff(zd,"BJdirk",BJdirk);
      }
      return 0;
   }

   zstat = zd->zstat;                                                            //  zdialog complete?
   if (! zstat) {
      BJedited++;                                                                //  no, manual edit was done
      return 0;
   }

   zd->zstat = 0;                                                                //  dialog may continue

   if (zstat == 1) {                                                             //  [browse]  do file-chooser dialog
      if (! BJedit_fchooser_zd)
         BJedit_fchooser("/home");
      return 0;
   }

   if (zstat == 2) {                                                             //  [clear]  clear include/exclude recs
      wclear(editwidget);
      BJedited++;
      return 0;
   }

   if (BJedit_fchooser_zd)                                                       //  kill file chooser dialog if active
      zdialog_free(BJedit_fchooser_zd);

   if (zstat != 3) {                                                             //  cancel or kill
      zdialog_free(zd);
      strcpy(BJdev,orgBJdev);                                                    //  restore original archive
      strcpy(BJdirk,orgBJdirk);
      BJdcc = strlen(BJdirk);                                                    //  v.5.6
      BJedited = 0;
      return 0;
   }

   if (! BJedited) {                                                             //  [done]
      zdialog_free(zd);                                                          //  no edits made
      return 0;
   }

   BJreset();                                                                    //  reset job data

   errmess = validate_archive();
   if (errmess) {
      wprintff(mLog," %s \n",errmess);
      nerrs++;
   }

   BJvmode = 0;
   zdialog_fetch(zd,"vincr",nn);                                                 //  get verify mode
   if (nn) BJvmode = 1;
   zdialog_fetch(zd,"vfull",nn);
   if (nn) BJvmode = 2;
   zdialog_fetch(zd,"vcomp",nn);
   if (nn) BJvmode = 3;
   
   zdialog_fetch(zd,"bjcop",BJcopyop);                                           //  get copy owner/perm option         5.3

   for (BJnnx = 0; BJnnx < maxnx; )                                              //  get include/exclude records
   {
      pp = wscanf(editwidget,ftf);
      if (! pp) break;

      errmess = parseNXrec(pp,rtype,fspec,vers,days);
      if (errmess) {
         wprintff(mLog,"%s \n *** %s \n",pp,errmess);
         nerrs++;
      }

      BJfspec[BJnnx] = fspec;
      BJrtype[BJnnx] = rtype;
      BJretNV[BJnnx] = vers;
      BJretND[BJnnx] = days;
      BJnnx++;
   }

   jobname = strrchr(BJfilespec,'/') + 1;
   snprintf(text,100,"%s   %s (*)",ukopp_title,jobname);                         //  (*) in title for edited job
   gtk_window_set_title(GTK_WINDOW(mWin),text);

   if (nerrs == 0) BJvalid = 1;                                                  //  valid job if no errors

   if (BJvalid) dGetFiles();                                                     //  validate file data                 6.0
   if (! BJvalid) {
      zmessageACK(mWin,"backup job has errors");
      return 0;
   }

   zdialog_free(zd);                                                             //  destroy dialog
   return 0;
}


//  choose archive device, mount point, archive directory

void BJedit_chooseArchive()                                                      //  overhauled v.5.4
{
   int  BJedit_chooseArchive_dialog_event(zdialog *zd, cchar *event);

   int         ii, zstat;
   char        text[300];
   zdialog     *zd;

   cchar       *tip1 = " Device and mount point are optional     \n"
                       " (will be mounted by Ukopp if needed)    ";

   cchar       *tip2 = " Archive directory is mandatory. Options:  \n"
                       "  1. The device mount point as above, or   \n"
                       "      a subdirectory under this directory  \n"
                       "  2. A valid directory if no device and    \n"
                       "      mount point are specified.           ";

   cchar       *errmess = 0;

/***
     ____________________________________________________
    |             Choose Archive Location                |
    |                                                    |
    |  Device and mount point are optional               |
    |  (will be mounted by Ukopp if needed)              |
    |                                                    |
    |  Device [____________________________|v]           |
    |  Mount Point [_________________________] [browse]  |
    |                                                    |
    |  Archive Directory is mandatory. Options:          |
    |   1. The device mount point as above, or           |
    |      a subdirectory under this directory.          |
    |   2. A valid directory if no device and            |
    |      mount point are specified.                    |
    |                                                    |
    |  Directory [___________________________] [browse]  |
    |                                                    |
    |                                  [ OK ]  [Cancel]  |
    |____________________________________________________|
    
***/    

   zd = zdialog_new("Choose Archive Location",mWin,"OK","cancel",null);

   zdialog_add_widget(zd,"label","labtip1","dialog",tip1,"space=3");

   zdialog_add_widget(zd,"hbox","hbdev","dialog");
   zdialog_add_widget(zd,"label","labdev","hbdev","Device","space=3");
   zdialog_add_widget(zd,"comboE","BJdev","hbdev",0,"space=3|expand");

   zdialog_add_widget(zd,"hbox","hbmp","dialog");
   zdialog_add_widget(zd,"label","labmp","hbmp","Mount Point","space=3");
   zdialog_add_widget(zd,"entry","BJmp","hbmp",0,"space=3|expand");
   zdialog_add_widget(zd,"button","browseMP","hbmp","browse","space=3");

   zdialog_add_widget(zd,"label","labtip2","dialog",tip2,"space=3");

   zdialog_add_widget(zd,"hbox","hbloc","dialog");
   zdialog_add_widget(zd,"label","labloc","hbloc","Directory","space=3");
   zdialog_add_widget(zd,"entry","BJdirk","hbloc",0,"space=3|expand");
   zdialog_add_widget(zd,"button","browseLoc","hbloc","browse","space=3");

   DevPoop();                                                                    //  refresh available devices

   for (ii = 0; ii < Ndisk; ii++)                                                //  load combo box with device poop
   {
      snprintf(text,300,"%s %s %s",diskdev[ii], diskmp[ii], diskdesc[ii]);
      zdialog_cb_app(zd,"BJdev",text);
   }
   
   if (*BJdev) {                                                                 //  device already specified
      for (ii = 0; ii < Ndisk; ii++)
         if (strmatch(BJdev,diskdev[ii])) break;                                 //  find device and mount point
      if (ii < Ndisk && *diskmp[ii] == '/') 
         strcpy(BJmp,diskmp[ii]);                                                //  use actual mount point if mounted
   }
   
   if (*BJmp && ! *BJdirk) strcpy(BJdirk,BJmp);                                  //  archive dirk unknown, use mount point
   BJdcc = strlen(BJdirk);                                                       //  v.5.6
      
   if (*BJdev) zdialog_stuff(zd,"BJdev",BJdev);                                  //  stuff known data into dialog
   if (*BJmp) zdialog_stuff(zd,"BJmp",BJmp);
   if (*BJdirk) zdialog_stuff(zd,"BJdirk",BJdirk);

   zdialog_resize(zd,350,0);
   zdialog_run(zd,BJedit_chooseArchive_dialog_event,"mouse");                    //  run dialog
   zstat = zdialog_wait(zd);
   if (zstat != 1) {
      zdialog_free(zd);
      return;
   }
   
   zdialog_fetch(zd,"BJdev",BJdev,40);                                           //  get user inputs
   zdialog_fetch(zd,"BJmp",BJmp,200);
   zdialog_fetch(zd,"BJdirk",BJdirk,200);
   BJdcc = strlen(BJdirk);                                                       //  v.5.6
   zdialog_free(zd);

   errmess = validate_archive();                                                 //  validate
   if (errmess) {
      wprintff(mLog," %s \n",errmess);
      zmessageACK(mWin,errmess);
   }

   return;
}


int BJedit_chooseArchive_dialog_event(zdialog *zd, cchar *event)
{
   int      ii, cc;
   char     *pp, *file, text[300];

   if (strmatch(event,"BJdev")) {
      zdialog_fetch(zd,"BJdev",text,300);                                        //  /dev/sdxx  /mountpoint  description
      pp = strstr(text,"/dev/");
      if (pp) {
         strncpy0(BJdev,pp,20);
         pp = BJdev + 5;
         while (*pp && *pp != ' ') pp++;
         *pp = 0;
      }
      else *BJdev = 0;
      zdialog_stuff(zd,"BJdev",BJdev);

      if (*BJdev) {                                                              //  if device is specified
         for (ii = 0; ii < Ndisk; ii++)
            if (strmatch(BJdev,diskdev[ii])) break;                              //  find device and mount point
         if (ii == Ndisk) return 1;
         if (*diskmp[ii] == '/') {
            strncpy0(BJmp,diskmp[ii],200);                                       //  if mounted, set mount point
            strcpy(BJdirk,BJmp);                                                 //    and directory = mount point
            BJdcc = strlen(BJdirk);                                              //  v.5.6
            zdialog_stuff(zd,"BJmp",BJmp);
            zdialog_stuff(zd,"BJdirk",BJdirk);
         }
      }
      BJedited++;
   }

   if (strmatch(event,"BJmp")) BJedited++;

   if (strmatch(event,"browseMP")) {
      file = zgetfile("mount point",MWIN,"folder",BJmp);
      if (file) {
         zdialog_stuff(zd,"BJmp",file);
         zdialog_stuff(zd,"BJdirk",file);
         zfree(file);
      }
      BJedited++;
   }

   if (strmatch(event,"BJdirk")) BJedited++;

   if (strmatch(event,"browseLoc")) {
      file = zgetfile("archive directory",MWIN,"folder",BJdirk);
      if (file) {
         zdialog_stuff(zd,"BJdirk",file);
         zfree(file);
      }
      BJedited++;
   }

   cc = strlen(BJmp);                                                            //  remove trailing '/'                5.6
   if (cc && BJmp[cc-1] == '/') {
      BJmp[cc-1] = 0;
      zdialog_stuff(zd,"BJmp",BJmp);
   }

   cc = strlen(BJdirk);
   if (cc && BJdirk[cc-1] == '/') {
      BJdirk[cc-1] = 0;
      BJdcc = strlen(BJdirk);                                                    //  v.5.6
      zdialog_stuff(zd,"BJdirk",BJdirk);
   }

   return 1;
}


//  file chooser dialog for backup job edit

int BJedit_fchooser(cchar *dirk)
{
   int BJedit_fchooser_event(zdialog *zd, const char *event);

   BJedit_fchooser_zd = zdialog_new("Choose Files for Backup",mWin,"Done",null);
   zdialog *zd = BJedit_fchooser_zd;

   zdialog_add_widget(zd,"frame","fr1","dialog",0,"expand");
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","space","hb1",0,"expand");
   zdialog_add_widget(zd,"button","incl","hb1","include","space=5");
   zdialog_add_widget(zd,"button","excl","hb1","exclude","space=5");
   zdialog_add_widget(zd,"check","showhf","hb1","Show hidden","space=10");
   zdialog_add_widget(zd,"label","space","hb2",0,"expand");
   zdialog_add_widget(zd,"label","lab1","hb2","Retain archive file versions: ");
   zdialog_add_widget(zd,"spin","vers","hb2","0|9999|1|0");
   zdialog_add_widget(zd,"label","lab2","hb2","   days: ");
   zdialog_add_widget(zd,"spin","days","hb2","0|9999|1|0");
   zdialog_add_widget(zd,"label","space","hb2",0,"space=5");

   fc_widget = gtk_file_chooser_widget_new(GTK_FILE_CHOOSER_ACTION_OPEN);
   GtkWidget *frame = zdialog_widget(zd,"fr1");
   gtk_container_add(GTK_CONTAINER(frame),fc_widget);
   gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(fc_widget),dirk);
   gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(fc_widget),1);
   gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(fc_widget),1);

   zdialog_stuff(zd,"showhf",1);
   zdialog_resize(zd,550,500);
   zdialog_run(zd,BJedit_fchooser_event);
   zdialog_wait(zd);
   zdialog_free(zd);
   BJedit_fchooser_zd = 0;
   return 0;
}


int BJedit_fchooser_event(zdialog *zd, const char *event)
{
   GSList      *flist = 0;
   STATB       statb;
   char        *file1, *file2;
   int         ii, err, showhf, vers, days;

   if (strmatch(event,"showhf"))                                                 //  show/hide hidden files
   {
      zdialog_fetch(zd,"showhf",showhf);
      gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(fc_widget),showhf);
   }

   if (strmatch(event,"incl") || strmatch(event,"excl"))                         //  include or exclude
   {
      flist = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(fc_widget));

      for (ii = 0; ; ii++)                                                       //  process selected files
      {
         file1 = (char *) g_slist_nth_data(flist,ii);
         if (! file1) break;

         file2 = zstrdup(file1,4);                                               //  extra space for wildcard
         g_free(file1);

         err = lstat(file2,&statb);
         if (err) {
            wprintff(mLog," *** error: %s  file: %s \n",strerror(errno),file2);
            continue;
         }

         if (S_ISDIR(statb.st_mode)) strcat(file2,"/*");                         //  if directory, append wildcard

         zdialog_fetch(zd,"vers",vers);                                          //  get corresp. retention specs
         zdialog_fetch(zd,"days",days);                                          //     from dialog

         if (strmatch(event,"incl")) {                                           //  include /filespec (vv,dd)
            if (vers || days)
               wprintff(editwidget,"include %s%s%d,%d%s\n",file2,RSEP1,vers,days,RSEP2);
            else  wprintff(editwidget,"include %s""\n",file2);
         }
         if (strmatch(event,"excl"))
            wprintff(editwidget,"exclude %s""\n",file2);

         zfree(file2);
         BJedited++;
      }

      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(fc_widget));
      g_slist_free(flist);
   }

   return 0;
}


/********************************************************************************/

//  parse a job file archive record and set BJdev, BJmp, BJdirk
//    archive  /dev/sdb2 /media/sdb2/  /media/sdb2/ukopp/
//    archive   (none)    (none)       /home/user/ukopp

cchar * parse_archive_rec(const char *text)                                      //  v.5.4
{
   int      Nth = 1;
   cchar    *pp;

   *BJdev = *BJmp = *BJdirk = BJdcc = 0;                                         //  reset archive data

   pp = strField(text,' ',Nth++);
   if (! pp || ! strmatch(pp,"archive"))                                         //  have "archive"
      return " *** invalid archive record";

   pp = strField(text,' ',Nth++);
   if (! pp) return " *** missing archive device or (none)";
   if (strmatchN(pp,"/dev/",5)) strncpy0(BJdev,pp,40);                           //  have /dev/sdxx

   pp = strField(text,' ',Nth++);
   if (! pp) return " *** missing mount point or (none)";
   if (*pp == '/') strncpy0(BJmp,pp,200);                                        //  have /mount/point

   pp = strField(text,' ',Nth++);
   if (! pp || *pp != '/') return " *** missing archive directory";
   strncpy0(BJdirk,pp,200);                                                      //  have /directory
   BJdcc = strlen(BJdirk);

   return 0;
}


//  validate archive location
//  if BJdev specified
//     check that it exists
//     if mounted, check mount point is BJmp
//     if not mounted, check BJmp exists and empty
//  if BJmp specified, check BJdirk is at or under BJmp
//  else check BJdirk is valid

cchar * validate_archive()                                                       //  v.5.4
{
   int         ii, err;
   char        *pp;
   DIR         *dirf;
   STATB       statb;
   struct dirent  *ppd;

   bFilesReset();                                                                //  clear files at archive location

   DevPoop();                                                                    //  refresh known device data
   
   if (*BJdev)                                                                   //  archive device is specified
   {
      wprintff(mLog," archive device: %s  mount point: %s \n",BJdev,BJmp);

      for (ii = 0; ii < Ndisk; ii++)
         if (strmatch(BJdev,diskdev[ii])) break;                                 //  check that device exists
      if (ii == Ndisk) return " *** device not found";
      
      if (! *BJmp) return " *** no mount point specified";                       //  check mount point is specified

      if (*diskmp[ii] == '/') {                                                  //  device is mounted
         if (strmatch(diskmp[ii],BJmp)) devMounted = 1;
         else return " *** device mounted somewhere else";                       //  must be at specified mount point
      }
      else {                                                                     //  device not mounted
         err = stat(BJmp,&statb);                                                //  determine if mount point exists
         if (! err) {
            if (! S_ISDIR(statb.st_mode))                                        //  yes, must be a directory
               return " *** mount point is not a directory";
            dirf = opendir(BJdirk);
            if (! dirf) return " *** cannot access mount point";                 //  must be accessible
            while (true) {
               ppd = readdir(dirf);
               if (! ppd) { closedir(dirf); break; }
               if (ppd->d_name[0] == '.') continue;
               closedir(dirf);
               return " *** mount point directory not empty";                    //  must be empty
            }
         }
      }
   }
   
   wprintff(mLog," archive directory: %s \n",BJdirk);
   if (! *BJdirk) return " *** archive directory not specified";
   
   if (! *BJdev) {                                                               //  if no archive device,              5.6
      err = stat(BJdirk,&statb);                                                 //    directory must be valid
      if (err || ! S_ISDIR(statb.st_mode))
         return " *** archive directory does not exist";
   }
   
   if (*BJmp) {
      pp = strstr(BJdirk,BJmp);
      if (pp != BJdirk) 
         return " *** archive directory not under mount point";
   }
   
   return 0;
}  
   

//  parse and validate a comment/include/exclude record for backup job
//  filespec* means a /path.../filename with wildcards
//  # comment (or blank line)
//  include  filespec*  [ (vers,days) ]
//  exclude  filespec*

cchar * parseNXrec(cchar *jobrec, int &rtype, char *&fspec, int &vers, int &days)
{
   int      nn, ii, Nth = 1;
   char     *pp1, *pp2, *pp3;

   rtype = vers = days = -1;
   fspec = null;

   pp1 = (char *) strField(jobrec,' ',Nth++);

   if (! pp1 || *pp1 == '#') {                                                   //  comment or blank line
      rtype = 1;
      if (pp1) fspec = zstrdup(pp1);
      else fspec = zstrdup("");
      return 0;
   }
   
   if (strmatch(pp1,"include") || strmatch(pp1,"exclude"))
   {
      if (strmatch(pp1,"include")) rtype = 2;
      else rtype = 3;

      pp1 = (char *) jobrec + 7;
      strTrim2(pp1);

      if (*pp1 == '"') {                                                         //  quoted filespec, remove quotes
         pp2 = pp1 + strlen(pp1) - 1;
         if (*pp2 != '"') return "filespec closing quote missing";
         pp1++;
         *pp2 = 0;
      }

      if (rtype == 2)                                                            //  include filespec [ (nn,nn) ]
      {
         if (*pp1 != '/') return "filespec missing /topdir/";
         pp2 = strchr(pp1+1,'/');
         if (! pp2) return "filespec missing /topdir/";
         pp3 = strchr(pp1,'*');
         if (pp3 && pp3 < pp2) return "wildcards in /topdir/ not allowed";
         pp3 = strchr(pp1,'?');
         if (pp3 && pp3 < pp2) return "wildcards in /topdir/ not allowed";

         vers = days = 0;
         pp2 = pp1 + strlen(pp1);
         if (strmatchN(pp2-1,VSEP2,1)) {                                         //  filespec ends with ")"
            for (ii = 6; ii < 12; ii++)                                          //  look back for " (" 
               if (strmatchN(pp2-ii,VSEP1,2)) break;
            if (ii < 12) {                                                       //  found possible " (nv,nd)"
               nn = sscanf(pp2-ii,VSEP1 "%d,%d" VSEP2,&vers,&days);              //  return retention vers & days
               if (nn == 2 && vers > 0 && days > 0) 
                  *(pp2-ii) = 0;                                                 //  remove " (nv,nd)" from filespec
            }
         }
      }

      fspec = zstrdup(pp1);
      return 0;
   }

   return "unrecognized record type";
}


//  parse and validate a comment/include/exclude record for a restore job
//  filespec* means a /path.../filename with wildcards
//  # comment (or blank line)
//  include  filespec [ (vers) ]
//  exclude  filespec [ (vers) ]
//  include  filespec*
//  exclude  filespec*

cchar * parseRXrec(cchar *jobrec, int &rtype, char *&fspec)                      //  v.5.4
{
   int      Nth = 1;
   char     *pp1, *pp2, *pp3;

   rtype = -1;
   fspec = null;

   pp1 = (char *) strField(jobrec,' ',Nth++);

   if (! pp1 || *pp1 == '#') {                                                   //  comment or blank line
      rtype = 1;
      if (pp1) fspec = zstrdup(pp1);
      else fspec = zstrdup("");
      return 0;
   }

   if (strmatch(pp1,"include") || strmatch(pp1,"exclude"))                       //  include or exclude filespec
   {
      if (strmatch(pp1,"include")) rtype = 2;
      else rtype = 3;

      pp1 = (char *) jobrec + 7;
      strTrim2(pp1);

      if (*pp1 == '"') {                                                         //  quoted filespec, remove quotes
         pp2 = pp1 + strlen(pp1) - 1;
         if (*pp2 != '"') return "filespec closing quote missing";
         pp1++;
         *pp2 = 0;
      }

      if (rtype == 2)
      {
         if (*pp1 != '/') return "filespec missing /topdir/";
         pp2 = strchr(pp1+1,'/');
         if (! pp2) return "filespec missing /topdir/";
         pp3 = strchr(pp1,'*');
         if (pp3 && pp3 < pp2) return "wildcards in /topdir/ not allowed";
         pp3 = strchr(pp1,'?');
         if (pp3 && pp3 < pp2) return "wildcards in /topdir/ not allowed";
      }

      fspec = zstrdup(pp1);
      return 0;
   }

   return "unrecognized record type";
}


//  parse a verify record: verify xxxxx

cchar * parseVerify(const char *text)
{
   const char     *pp;

   BJvmode = 0;

   pp = strField(text,' ',1);
   if (! pp || ! strmatch(pp,"verify")) return "bad verify record";

   pp = strField(text,' ',2);
   if (! pp) return "missing verify type";

   BJvmode = -1;
   if (strmatch(pp,"none")) BJvmode = 0;
   if (strmatchN(pp,"incr",4)) BJvmode = 1;
   if (strmatch(pp,"full")) BJvmode = 2;
   if (strmatchN(pp,"comp",4)) BJvmode = 3;
   if (BJvmode >= 0) return 0;

   BJvmode = 0;
   return "bad verify mode";
}


/********************************************************************************/

//  Delete backup files exceeding age and version limits.
//  Copy new and modified disk files to archive location.

int Backup(cchar *menu)
{
   char        message[100];
   int         vmode = 0, terrs = 0;
   int         err, ii, jj, yn;
   int         upvers = 0, deleted = 0;
   char        disp, *dfile = 0;
   const char  *errmess = 0;
   double      bsecs, bbytes, bspeed;
   double      time0;

   if (! BJvalid) {
      zmessageACK(mWin,"backup job has errors (re-open or edit job)");
      return 0;
   }

   if (! mount(0)) return 0;                                                     //  validate and mount archive

   Report("diffs summary");                                                      //  refresh all file data, report diffs

   if (*BJdev) {
      yn = zmessageYN(mWin,
               "archive device: %s \n"
               "mount point: %s \n"                                              //  confirm archive location
               "archive directory: %s \n"
               "%d new and modified files (%s) will be added \n"
               "%d deleted files will be purged \n"
               "%d expired old file versions will be purged \n"                  //  v.5.8
               "continue?",
               BJdev,BJmp,BJdirk,(nnew+nmod),formatKBMB(Mbytes,3),ndel,nexpv);
   }
   else {
      yn = zmessageYN(mWin,
               "archive directory: %s \n"
               "%d new and modified files (%s) will be added \n"
               "%d deleted files will be purged \n"
               "%d expired old file versions will be purged \n"                  //  v.5.8
               "continue?",
               BJdirk,(nnew+nmod),formatKBMB(Mbytes,3),ndel,nexpv);              //  bugfix  v.6.2
   }

   if (! yn) return 0;

   snprintf(message,99,"\n""begin backup \n");
   wprintxx(mLog,0,message,1);
   wprintff(mLog," files: %d  bytes: %s \n",Mfiles,formatKBMB(Mbytes,3));        //  files and bytes to copy

   wprintff(mLog," using archive: %s %s %s \n",BJdev,BJmp,BJdirk);

   if (strmatch(menu,"backup only")) vmode = 0;                                  //  backup command, no auto verify
   if (strmatch(menu,"run job")) vmode = BJvmode;

   wprintff(mLog," assign new version numbers to modified archive files \n"
                 " and purge expired old versions from archive location \n\n");

   gdk_window_set_cursor(mLogwin,watchcursor);                                   //  busy cursor

   for (ii = 0; ii < Bnf; ii++)                                                  //  scan files at archive location
   {
      disp = Brec[ii].disp;
      dfile = Brec[ii].file;

      errmess = null;

      if (disp == 'm' || disp == 'd') {                                          //  modified or deleted,
         errmess = setnextVersion(Brec[ii]);                                     //  rename to next version number
         Brec[ii].err = -1;                                                      //  mark file gone
         if (disp =='m') upvers++;                                               //  update counts
         if (disp =='d') deleted++;
      }

      if (! errmess) errmess = purgeVersions(Brec[ii],1);                        //  purge expired file versions

      if (errmess) {
         wprintff(mLog," %s \n *** %s \n",dfile,errmess);                        //  log error
         terrs++;
         if (terrs > 100) goto backup_fail;
      }
   }

   wprintff(mLog," %d archive files were assigned new versions \n",upvers);
   wprintff(mLog," %d archive files were deleted \n",deleted);
   wprintff(mLog," %d expired old file versions (%s) were purged \n\n",Pfiles,formatKBMB(Pbytes,3));
   Pbytes = Pfiles = 0;

   start_timer(time0);                                                           //  start timer
   bbytes = Mbytes;

   BJstore(TFjobfile);                                                           //  copy job file to temp file
   writeDT();                                                                    //  create date-time temp file

   wprintff(mLog,-2," %s \n",BD_JOBFILE);
   errmess = copyFile(TFjobfile,BD_JOBFILE,2,BJcopyop);                          //  copy job file to archive location
   if (errmess) goto backup_fail;

   wprintff(mLog,-2," %s \n",BD_DATETIME);
   errmess = copyFile(TFdatetime,BD_DATETIME,2,BJcopyop);                        //  copy date-time file
   if (errmess) goto backup_fail;

   wprintff(mLog," copying new and modified files from disk to archive location \n\n");

   for (ii = 0; ii < Dnf; ii++)                                                  //  scan all disk files
   {
      disp = Drec[ii].disp;
      dfile = Drec[ii].file;
      Drec[ii].finc = 0;                                                         //  not included yet

      if (disp == 'n' || disp == 'm')                                            //  new or modified file
      {
         wprintff(mLog," %s \n",dfile);
         errmess = copyFile(dfile,dfile,2,BJcopyop);                             //  copy disk file to archive
         if (errmess) {
            Drec[ii].err = 1;                                                    //  copy failed
            wprintff(mLog," *** %s \n",errmess);                                 //  log error
            terrs++;
            if (terrs > 100) goto backup_fail;
         }
         else {                                                                  //  copy OK
            Drec[ii].finc = 1;                                                   //  set included file flag
         }

         if (checkKillPause()) goto backup_fail;                                 //  killed by user
      }

      jj = Drec[ii].bindx;                                                       //  purge last version now
      if (jj >= 0) purgeVersions(Brec[jj],0);                                    //  bugfix
   }

   if (terrs) wprintff(mLog," *** %d files had backup errors \n",terrs);

   synch_poop("backup");                                                         //  synch owner and permissions data

   snprintf(message,99,"\n""flushing file cache \n");
   wprintxx(mLog,0,message,1);
   if (getuid() != 0)                                                            //  check for permission               6.2
      wprintff(mLog," *** no root permissions, flush not done \n");
   else {
      err = system("sync; echo 2 > /proc/sys/vm/drop_caches");                   //  flush the file cache
      if (err) wprintff(mLog," *** error: %s \n",wstrerror(err));
   }

   bsecs = get_timer(time0);                                                     //  output perf. statistics
   format_seconds(bsecs,message);                                                //  hh:mm:ss       v.5.4
   wprintff(mLog,"\n");
   wprintff(mLog," backup time: %s \n",message);
   bspeed = bbytes/mega/bsecs;
   wprintff(mLog," backup speed: %.1f MB/sec \n",bspeed);
   wprintff(mLog," backup complete \n");

   if (vmode)                                                                    //  do verify if requested
   {
      wprintff(mLog,"\n");
      sleep(2);

      if (vmode == 1) Verify("incr");
      else if (vmode == 2) Verify("full");
      else if (vmode == 3) Verify("comp");

      sprintf(message," %d files had backup errors \n",terrs);                   //  repeat backup status
      wprintxx(mLog,0,message,1);

      terrs = fverrs + fcerrs;
      sprintf(message," %d files had verify errors \n",terrs);                   //  add verify status
      wprintxx(mLog,0,message,1);
   }

   wprintff(mLog," ready \n");

   if (ukoppMounted) unmount(0);                                                 //  leave unmounted

   gdk_window_set_cursor(mLogwin,0);                                             //  normal cursor
   return 0;

backup_fail:
   if (terrs > 100) wprintff(mLog," too many errors, giving up \n");
   else if (errmess) wprintff(mLog," %s \n",errmess);
   wprintxx(mLog,0," *** BACKUP FAILED \n",1);

   bFilesReset();
   killFlag = 0;
   gdk_window_set_cursor(mLogwin,0);                                             //  normal cursor
   return 0;
}


/********************************************************************************/

//  synchronize disk and archive files                                           //  v.25
//  bi-directional copy of new and newer files

int Synch(cchar *menu)
{
   int         ii, yn, dii, bii, comp;
   char        disp, *dfile = 0;
   time_t      btime, dtime;
   const char  *errmess = 0;

   if (! BJvalid) {
      zmessageACK(mWin,"backup job has errors (re-open or edit job)");
      return 0;
   }

   if (! mount(0)) return 0;                                                     //  validate and mount archive

   yn = zmessageYN(mWin,"archive: %s %s %s \n continue?",BJdev,BJmp,BJdirk);
   if (! yn) return 0;
   wprintff(mLog," using archive: %s %s %s \n",BJdev,BJmp,BJdirk);

   dGetFiles();                                                                  //  get disk files of backup job
   if (bGetFiles() < 0) goto synch_exit;                                         //  get files in archive location
   setFileDisps();                                                               //  compare and set dispositions

   wprintff(mLog,"\n begin synchronize \n");

   gdk_window_set_cursor(mLogwin,watchcursor);                                   //  busy cursor

   BJstore(TFjobfile);                                                           //  copy job file to temp file
   writeDT();                                                                    //  create date-time temp file

   wprintff(mLog,-2," %s \n",BD_JOBFILE);
   errmess = copyFile(TFjobfile,BD_JOBFILE,2,BJcopyop);                          //  copy job file to archive location
   if (errmess) goto synch_exit;

   wprintff(mLog,-2," %s \n",BD_DATETIME);
   errmess = copyFile(TFdatetime,BD_DATETIME,2,BJcopyop);                        //  copy date-time file
   if (errmess) goto synch_exit;

   for (ii = 0; ii < Dnf; ii++)                                                  //  copy new disk files >> archive loc.
   {
      disp = Drec[ii].disp;
      dfile = Drec[ii].file;
      if (disp != 'n') continue;
      wprintff(mLog," disk >> archive: %s \n",dfile);
      errmess = copyFile(dfile,dfile,2,BJcopyop);
      if (errmess) wprintff(mLog," *** %s \n",errmess);
      else Drec[ii].finc = 1;
      if (checkKillPause()) goto synch_exit;                                     //  killed by user
   }

   for (ii = 0; ii < Bnf; ii++)                                                  //  copy new archive files >> disk
   {                                                                             //  (aka "deleted" disk files)
      disp = Brec[ii].disp;
      dfile = Brec[ii].file;
      if (disp != 'd') continue;
      wprintff(mLog," archive >> disk: %s \n",dfile);
      errmess = copyFile(dfile,dfile,1,0);
      if (errmess) wprintff(mLog," *** %s \n",errmess);
      else Brec[ii].finc = 1;
      if (checkKillPause()) goto synch_exit;
   }

   dii = bii = 0;

   while ((dii < Dnf) || (bii < Bnf))                                            //  scan disk and archive files parallel
   {
      if ((dii < Dnf) && (bii == Bnf)) comp = -1;
      else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
      else comp = strcmp(Drec[dii].file, Brec[bii].file);

      if (comp < 0) { dii++; continue; }                                         //  next disk file
      if (comp > 0) { bii++; continue; }                                         //  next archive file

      disp = Drec[dii].disp;
      dfile = Drec[dii].file;

      if (disp == 'm')                                                           //  screen for modified status
      {
         btime = int(Brec[bii].mtime);
         dtime = int(Drec[dii].mtime);

         if (btime > dtime) {                                                    //  copy newer archive file >> disk
            wprintff(mLog," archive >> disk: %s \n",dfile);
            errmess = copyFile(dfile,dfile,1,0);
            if (errmess) wprintff(mLog," *** %s \n",errmess);
            else Brec[bii].finc = 1;
         }

         else {                                                                  //  copy newer disk file >> archive
            wprintff(mLog," disk >> archive: %s \n",dfile);
            errmess = copyFile(dfile,dfile,2,BJcopyop);
            if (errmess) wprintff(mLog," *** %s \n",errmess);
            else Drec[dii].finc = 1;
         }
      }

      dii++;                                                                     //  next disk and archive files
      bii++;

      if (checkKillPause()) goto synch_exit;                                     //  killed by user
   }

   errmess = null;
   synch_poop("synch");                                                          //  synch owner and permissions data

   Verify("incremental");                                                        //  verify all files copied

synch_exit:
   if (errmess) wprintff(mLog," *** %s \n",errmess);
   wprintff(mLog," ready \n");
   killFlag = 0;
   gdk_window_set_cursor(mLogwin,0);                                             //  normal cursor
   return 0;
}


/********************************************************************************/

//  verify integrity of archive files

int Verify(cchar *menu)
{
   int         ii, vers, comp, vfiles;
   int         dfiles1 = 0, dfiles2 = 0;
   char        filespec[XFCC];
   char        message[100];
   const char  *errmess = 0;
   double      secs, dcc1, vbytes, vspeed;
   double      mtime, diff;
   double      time0;
   STATB       statb;

   vfiles = fverrs = fcerrs = 0;
   vbytes = 0.0;
   if (! mount(0)) return 0;                                                     //  validate and mount archive

   start_timer(time0);

   gdk_window_set_cursor(mLogwin,watchcursor);                                   //  busy cursor

   if (strmatchN(menu,"incremental",4))                                          //  verify new/modified files only
   {
      wprintxx(mLog,0,"\n""Verify files copied in prior backup or synch \n",1);

      for (ii = 0; ii < Dnf; ii++)                                               //  scan disk file list
      {
         if (! Drec[ii].finc) continue;                                          //  file included in last backup
         strncpy0(filespec,Drec[ii].file,XFCC-1);
         wprintff(mLog,"  %s \n",filespec);                                      //  output filespec

         errmess = checkFile(filespec,1,dcc1);                                   //  compare disk/archive files, get length
         if (errmess) {
            wprintff(mLog,"  *** %s \n\n",errmess);                              //  log and count errors
            if (strstr(errmess,"compare")) fcerrs++;                             //  archive - disk compare failure
            else  fverrs++;
         }

         vfiles++;                                                               //  count files and bytes
         vbytes += dcc1;
         if (fverrs + fcerrs > 100) goto verify_exit;

         if (checkKillPause()) goto verify_exit;                                 //  killed by user
      }

      for (ii = 0; ii < Bnf; ii++)                                               //  scan archive file list
      {
         if (! Brec[ii].finc) continue;                                          //  file included in last backup
         strncpy0(filespec,Brec[ii].file,XFCC-1);
         wprintff(mLog,"  %s \n",filespec);                                      //  output filespec

         errmess = checkFile(filespec,1,dcc1);                                   //  compare disk/archive files, get length
         if (errmess) {
            wprintff(mLog,"  *** %s \n\n",errmess);                              //  log and count errors
            if (strstr(errmess,"compare")) fcerrs++;                             //  archive - disk compare failure
            else  fverrs++;
         }

         vfiles++;                                                               //  count files and bytes
         vbytes += dcc1;
         if (fverrs + fcerrs > 100) goto verify_exit;

         if (checkKillPause()) goto verify_exit;                                 //  killed by user
      }
   }

   if (strmatch(menu,"full"))                                                    //  verify all files are readable
   {
      wprintxx(mLog,0,"\n""Read and verify ALL archive files \n\n",1);

      bGetFiles();                                                               //  get all files at archive location
      wprintff(mLog," %d archive files \n",Bnf);
      if (! Bnf) goto verify_exit;

      for (ii = 0; ii < Bnf; ii++)                                               //  scan archive file list
      {
         strncpy0(filespec,Brec[ii].file,XFCC-10);                               //  /directory.../filename

         if (Brec[ii].err == 0)
         {                                                                       //  check current file
            wprintff(mLog,-2," %s \n",filespec);
            errmess = checkFile(filespec,0,dcc1);                                //  verify file, get length
            if (errmess) {
               wprintff(mLog,-1," *** %s \n",errmess);                           //  log and count error
               wprintff(mLog,"\n");
               fverrs++;
            }
            vfiles++;                                                            //  count files and bytes
            vbytes += dcc1;
         }

         if (Brec[ii].lover)
         for (vers = Brec[ii].lover; vers <= Brec[ii].hiver; vers++)             //  check previous versions
         {
            setFileVersion(filespec,vers);                                       //  append version if > 0
            wprintff(mLog,-2," %s \n",filespec);
            errmess = checkFile(filespec,0,dcc1);                                //  verify file, get length
            if (errmess) {
               wprintff(mLog,-1," *** %s \n",errmess);                           //  log and count error
               wprintff(mLog,"\n");
               fverrs++;
            }
            vfiles++;                                                            //  count files and bytes
            vbytes += dcc1;
         }

         if (fverrs + fcerrs > 100) goto verify_exit;

         if (checkKillPause()) goto verify_exit;                                 //  killed by user
      }
   }

   if (strmatchN(menu,"compare",4))                                              //  compare archive files to disk files
   {
      wprintxx(mLog,0,"\n Read and verify ALL archive files. \n",1);
      wprintff(mLog," Compare to correspending disk files (if present). \n\n");

      bGetFiles();                                                               //  get all files at archive location
      wprintff(mLog," %d archive files \n",Bnf);
      if (! Bnf) goto verify_exit;

      for (ii = 0; ii < Bnf; ii++)                                               //  scan archive file list
      {
         strncpy0(filespec,Brec[ii].file,XFCC-10);                               //  /directory.../filename

         if (Brec[ii].err == 0)
         {                                                                       //  check current file
            comp = 0;
            if (lstat(filespec,&statb) == 0) {                                   //  corresponding disk file exists
               mtime = statb.st_mtime + statb.st_mtim.tv_nsec * nano;
               diff = fabs(mtime - Brec[ii].mtime);                              //  compare disk and archive mod times
               if (diff < MODTIMETOLR) comp = 1;                                 //  equal within file system resolution
               dfiles1++;                                                        //  count matching disk names
               dfiles2 += comp;                                                  //  count matching mod times
            }

            wprintff(mLog,-2," %s \n",filespec);
            errmess = checkFile(filespec,comp,dcc1);                             //  verify, get length, compare disk
            if (errmess) {
               wprintff(mLog,-1," *** %s \n",errmess);                           //  log and count error
               wprintff(mLog,"\n");
               if (strstr(errmess,"compare")) fcerrs++;                          //  archive - disk compare failure
               else  fverrs++;
            }
            vfiles++;                                                            //  count files and bytes
            vbytes += dcc1;
         }

         if (Brec[ii].lover)
         for (vers = Brec[ii].lover; vers <= Brec[ii].hiver; vers++)             //  check previous versions
         {
            setFileVersion(filespec,vers);                                       //  append version if > 0
            wprintff(mLog,-2," %s \n",filespec);
            errmess = checkFile(filespec,0,dcc1);                                //  verify file, get length
            if (errmess) {
               wprintff(mLog,-1," *** %s \n",errmess);                           //  log and count error
               wprintff(mLog,"\n");
               fverrs++;
            }
            vfiles++;                                                            //  count files and bytes
            vbytes += dcc1;
         }

         if (checkKillPause()) goto verify_exit;                                 //  killed by user
         if (fverrs + fcerrs > 100) goto verify_exit;
      }
   }

   wprintff(mLog," archive files: %d  (%s) \n",vfiles,formatKBMB(vbytes,3));
   wprintff(mLog," archive file read errors: %d \n",fverrs);

   if (strmatchN(menu,"incremental",4))
      wprintff(mLog," compare failures: %d \n",fcerrs);

   if (strmatchN(menu,"compare",4)) {
      wprintff(mLog," matching disk names: %d  mod times: %d \n",dfiles1,dfiles2);
      wprintff(mLog," compare failures: %d \n",fcerrs);
   }

   secs = get_timer(time0);
   format_seconds(secs,message);                                                 //  hh:mm:ss  v.5.4
   wprintff(mLog," verify time: %s \n",message);
   vspeed = vbytes/mega/secs;
   wprintff(mLog," verify speed: %.1f MB/sec \n",vspeed);

verify_exit:
   if (fverrs + fcerrs) wprintxx(mLog,0," *** THERE WERE ERRORS *** \n",1);
   else wprintxx(mLog,0," NO ERRORS \n",1);
   wprintff(mLog," ready \n");
   killFlag = 0;
   gdk_window_set_cursor(mLogwin,0);                                             //  normal cursor
   return 0;
}


/********************************************************************************/

//  various kinds of reports

int Report(cchar *menu)
{
   char        *fspec1;
   char        fspec2[200], bfile[XFCC];
   char        *pslash, *pdirk, ppdirk[XFCC];
   char        header[100];
   int         ii, kfiles, knew, kdel, kmod;
   int         dii, bii, comp, err;
   double      nbytes, mb1, mb2, fage;
   int         vers, lover, hiver;
   int         age, loage, hiage;
   struct tm   tmdt;
   time_t      btime, dtime;
   char        bmod[20], dmod[20];
   const char  *copy;
   STATB       statb;

   //  get all disk files in backup job
   //  report file and byte counts per include and exclude record

   if (strmatch(menu, "get disk files"))
   {
      dGetFiles();                                                               //  get all files on disk

      wprintxx(mLog,0,"\n""  files    bytes    filespec    retention (vers, days) \n",1);

      for (ii = 0; ii < BJnnx; ii++) {                                           //  formatted report
         if (BJfspec[ii]) {
            if (BJfiles[ii]) {
               if (BJrtype[ii] == 2)                                             //  include: add retention
                  wprintff(mLog," %6d %9s   %s  (%d, %d) \n",
                     BJfiles[ii],formatKBMB(BJbytes[ii],3),BJfspec[ii],BJretNV[ii],BJretND[ii]);
               else
                  wprintff(mLog," %6d %9s   %s \n",BJfiles[ii],formatKBMB(BJbytes[ii],3),BJfspec[ii]);
            }
            else if (BJrtype[ii] > 1) {
               wprintxx(mLog,0,"   NONE    ",1);
               wprintff(mLog,"         %s \n",BJfspec[ii]);
            }
            else
               wprintff(mLog,"                    %s \n",BJfspec[ii]);
         }
      }

      wprintff(mLog," %6d %9s   TOTALS \n", Dnf, formatKBMB(Dbytes,3));
      goto report_exit;
   }

   //  report disk / archive differences: new, modified, and deleted files

   if (strmatch(menu, "diffs summary"))
   {
      dGetFiles();
      if (! BJvalid) {                                                           //  6.0
         zmessageACK(mWin,"backup job has errors (re-open or edit job)");
         return 0;
      }

      if (bGetFiles() < 0) goto report_exit;
      setFileDisps();

      wprintff(mLog,"\n disk files: %d  archive files: %d \n",Dnf,Bnf);
      wprintxx(mLog,0,"\n Differences between files on disk and archive files: \n",1);
      wprintff(mLog," %6d  disk files not found on archive (new files) \n",nnew);
      wprintff(mLog," %6d  files with different data (modified files) \n",nmod);
      wprintff(mLog," %6d  archive files not found on disk (deleted files) \n",ndel);
      wprintff(mLog," %6d  files with identical data (unchanged files) \n",nunc);
      wprintff(mLog," %6d  expired old file versions (to be purged) \n",nexpv);                                     //  5.8
      wprintff(mLog," Total differences: %d files (%s new + modified) \n\n",Mfiles,formatKBMB(Mbytes,3));
      goto report_exit;
   }

   //  report disk / archive differences per directory level

   if (strmatch(menu, "diffs by directory"))
   {
      dGetFiles();                                                               //  6.0
      if (! BJvalid) {
         zmessageACK(mWin,"backup job has errors (re-open or edit job)");
         return 0;
      }

      if (bGetFiles() < 0) goto report_exit;
      setFileDisps();

      SortFileList((char *) Drec, sizeof(dfrec), Dnf, 'D');                      //  re-sort, directories first
      SortFileList((char *) Brec, sizeof(bfrec), Bnf, 'D');

      wprintxx(mLog,0,"\n differences by directory \n",1);

      wprintxx(mLog,0,"   new   mod   del   bytes   directory \n",1);

      nbytes = kfiles = knew = kmod = kdel = 0;
      dii = bii = 0;

      while ((dii < Dnf) || (bii < Bnf))                                         //  scan disk and archive files parallel
      {
         if ((dii < Dnf) && (bii == Bnf)) comp = -1;
         else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
         else comp = filecomp(Drec[dii].file, Brec[bii].file);

         if (comp > 0) pdirk = Brec[bii].file;                                   //  get disk or archive file
         else pdirk = Drec[dii].file;

         pslash = (char *) strrchr(pdirk,'/');                                   //  isolate directory
         if (pslash) *pslash = 0;
         if (! strmatch(pdirk,ppdirk)) {                                         //  if directory changed, output
            if (kfiles > 0)                                                      //    totals from prior directory
               wprintff(mLog," %5d %5d %5d %8s  %s \n",
                       knew,kmod,kdel,formatKBMB(nbytes,3),ppdirk);
            nbytes = kfiles = knew = kmod = kdel = 0;                            //  reset totals
            strcpy(ppdirk,pdirk);                                                //  start new directory
         }
         if (pslash) *pslash = '/';

         if (comp < 0) {                                                         //  unmatched disk file: new
            knew++;                                                              //  count new files
            kfiles++;
            nbytes += Drec[dii].size;
            dii++;
         }

         else if (comp > 0) {                                                    //  unmatched archive file
            if (Brec[bii].disp == 'd') {
               kdel++;                                                           //  count deleted files
               kfiles++;
            }
            bii++;
         }

         else if (comp == 0) {                                                   //  file present on disk and archive
            if (Drec[dii].disp == 'm') kmod++;                                   //  count modified files
            if (Drec[dii].disp == 'n') knew++;                                   //  count new files (backup disp is 'v')
            if (Drec[dii].disp != 'u') {
               kfiles++;                                                         //  count unless unchanged
               nbytes += Drec[dii].size;
            }
            dii++;
            bii++;
         }
      }

      if (kfiles > 0) wprintff(mLog," %5d %5d %5d %s  %s \n",                    //  totals from last directory
                              knew,kmod,kdel,formatKBMB(nbytes,3),ppdirk);

      SortFileList((char *) Drec, sizeof(dfrec), Dnf, 'A');                      //  restore straight ascii sort
      SortFileList((char *) Brec, sizeof(bfrec), Bnf, 'A');
      goto report_exit;
   }

   //  report disk / archive differences by file status and directory

   if (strmatch(menu, "diffs by file status"))
   {
      dGetFiles();
      if (! BJvalid) {
         zmessageACK(mWin,"backup job has errors (re-open or edit job)");        //  6.0
         return 0;
      }

      if (bGetFiles() < 0) goto report_exit;
      setFileDisps();

      SortFileList((char *) Drec, sizeof(dfrec), Dnf, 'D');                      //  re-sort, directories first
      SortFileList((char *) Brec, sizeof(bfrec), Bnf, 'D');

      wprintxx(mLog,0,"\n new files by directory \n",1);                         //  report new files
      wprintxx(mLog,0,"  files   bytes   directory \n",1);

      nbytes = knew = 0;
      dii = bii = 0;

      while ((dii < Dnf) || (bii < Bnf))                                         //  scan disk and archive files parallel
      {
         if ((dii < Dnf) && (bii == Bnf)) comp = -1;
         else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
         else comp = filecomp(Drec[dii].file, Brec[bii].file);

         if (comp > 0) pdirk = Brec[bii].file;                                   //  get disk or archive file
         else pdirk = Drec[dii].file;

         pslash = (char *) strrchr(pdirk,'/');                                   //  isolate directory
         if (pslash) *pslash = 0;
         if (! strmatch(pdirk,ppdirk)) {                                         //  if directory changed, output
            if (knew > 0)                                                        //    totals from prior directory
               wprintff(mLog," %6d %8s  %s \n",
                       knew,formatKBMB(nbytes,3),ppdirk);
            nbytes = knew = 0;                                                   //  reset totals
            strcpy(ppdirk,pdirk);                                                //  start new directory
         }
         if (pslash) *pslash = '/';

         if (comp < 0) {                                                         //  unmatched disk file: new
            knew++;                                                              //  count new files
            nbytes += Drec[dii].size;
            dii++;
         }

         else if (comp > 0)                                                      //  unmatched archive file: deleted
            bii++;

         else if (comp == 0) {                                                   //  file present on disk and archive
            if (Drec[dii].disp == 'n') {                                         //  count new files (backup disp is 'v')
               knew++;
               nbytes += Drec[dii].size;
            }
            dii++;
            bii++;
         }
      }

      if (knew > 0) wprintff(mLog," %6d %8s  %s \n",                             //  totals from last directory
                            knew,formatKBMB(nbytes,3),ppdirk);

      wprintxx(mLog,0,"\n modified files by directory \n",1);                    //  report modified files
      wprintxx(mLog,0,"  files   bytes   directory \n",1);

      nbytes = kmod = 0;
      dii = bii = 0;

      while ((dii < Dnf) || (bii < Bnf))                                         //  scan disk and archive files parallel
      {
         if ((dii < Dnf) && (bii == Bnf)) comp = -1;
         else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
         else comp = filecomp(Drec[dii].file, Brec[bii].file);

         if (comp > 0) pdirk = Brec[bii].file;                                   //  get disk or archive file
         else pdirk = Drec[dii].file;

         pslash = (char *) strrchr(pdirk,'/');                                   //  isolate directory
         if (pslash) *pslash = 0;
         if (! strmatch(pdirk,ppdirk)) {                                         //  if directory changed, output
            if (kmod > 0)                                                        //    totals from prior directory
               wprintff(mLog," %6d %8s  %s \n",
                       kmod,formatKBMB(nbytes,3),ppdirk);
            nbytes = kmod = 0;                                                   //  reset totals
            strcpy(ppdirk,pdirk);                                                //  start new directory
         }
         if (pslash) *pslash = '/';

         if (comp < 0)                                                           //  unmatched disk file: new
            dii++;

         else if (comp > 0)                                                      //  unmatched archive file: deleted
            bii++;

         else if (comp == 0) {                                                   //  file present on disk and archive
            if (Drec[dii].disp == 'm') {                                         //  count modified files
               kmod++;
               nbytes += Drec[dii].size;
            }
            dii++;
            bii++;
         }
      }

      if (kmod > 0) wprintff(mLog," %6d %8s  %s \n",                             //  totals from last directory
                            kmod,formatKBMB(nbytes,3),ppdirk);

      wprintxx(mLog,0,"\n deleted files by directory \n",1);                     //  report deleted files
      wprintxx(mLog,0,"  files   bytes   directory \n",1);

      nbytes = kdel = 0;
      dii = bii = 0;

      while ((dii < Dnf) || (bii < Bnf))                                         //  scan disk and archive files parallel
      {
         if ((dii < Dnf) && (bii == Bnf)) comp = -1;
         else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
         else comp = filecomp(Drec[dii].file, Brec[bii].file);

         if (comp > 0) pdirk = Brec[bii].file;                                   //  get disk or archive file
         else pdirk = Drec[dii].file;

         pslash = (char *) strrchr(pdirk,'/');                                   //  isolate directory
         if (pslash) *pslash = 0;
         if (! strmatch(pdirk,ppdirk)) {                                         //  if directory changed, output
            if (kdel > 0)                                                        //    totals from prior directory
               wprintff(mLog," %6d %8s  %s \n",
                       kdel,formatKBMB(nbytes,3),ppdirk);
            nbytes = kdel = 0;                                                   //  reset totals
            strcpy(ppdirk,pdirk);                                                //  start new directory
         }
         if (pslash) *pslash = '/';

         if (comp < 0)                                                           //  unmatched disk file: new
            dii++;

         else if (comp > 0) {                                                    //  unmatched archive file: deleted
            if (Brec[bii].disp == 'd') {
               kdel++;                                                           //  count deleted files
               nbytes += Brec[bii].size;
            }
            bii++;
         }

         else if (comp == 0) {                                                   //  file present on disk and archive
            dii++;
            bii++;
         }
      }

      if (kdel > 0) wprintff(mLog," %6d %8s  %s \n",                             //  totals from last directory
                            kdel,formatKBMB(nbytes,3),ppdirk);

      SortFileList((char *) Drec, sizeof(dfrec), Dnf, 'A');                      //  restore straight ascii sort
      SortFileList((char *) Brec, sizeof(bfrec), Bnf, 'A');
      goto report_exit;
   }

   //  report disk / archive differences by file

   if (strmatch(menu, "diffs by file"))
   {
      dGetFiles();
      if (! BJvalid) {
         zmessageACK(mWin,"backup job has errors (re-open or edit job)");        //  6.0
         return 0;
      }

      if (bGetFiles() < 0) goto report_exit;
      setFileDisps();

      wprintxx(mLog,0,"\n Detailed list of disk:archive differences: \n",1);

      snprintf(header,99,"\n %d disk files not found on archive \n",nnew);
      wprintxx(mLog,0,header,1);

      for (ii = 0; ii < Dnf; ii++)
      {
         if (Drec[ii].disp != 'n') continue;
         wprintff(mLog,"  %s \n",Drec[ii].file);
      }

      snprintf(header,99,"\n %d archive files not found on disk \n",ndel);
      wprintxx(mLog,0,header,1);

      for (ii = 0; ii < Bnf; ii++)
      {
         if (Brec[ii].disp != 'd') continue;
         wprintff(mLog,"  %s \n",Brec[ii].file);
      }

      snprintf(header,99,"\n %d files with different data \n",nmod);
      wprintxx(mLog,0,header,1);

      wprintxx(mLog,0,"  archive mod date  copy  disk mod date     filespec \n",1);

      dii = bii = 0;

      while ((dii < Dnf) || (bii < Bnf))                                         //  scan disk and archive files parallel
      {
         if ((dii < Dnf) && (bii == Bnf)) comp = -1;
         else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
         else comp = strcmp(Drec[dii].file, Brec[bii].file);

         if (comp < 0) { dii++; continue; }                                      //  next disk file
         if (comp > 0) { bii++; continue; }                                      //  next archive file

         if (Drec[dii].disp == 'm')                                              //  screen for modified status
         {
            btime = int(Brec[bii].mtime);                                        //  mod time on archive
            dtime = int(Drec[dii].mtime);                                        //  mod time on disk

            copy = "<<<<";                                                       //  copy direction, disk to archive
            if (btime > dtime) copy = "!!!!";                                    //  flag if archive to disk

            tmdt = *localtime(&btime);
            snprintf(bmod,19,"%4d.%02d.%02d-%02d:%02d",tmdt.tm_year+1900,
                     tmdt.tm_mon+1,tmdt.tm_mday,tmdt.tm_hour,tmdt.tm_min);

            tmdt = *localtime(&dtime);
            snprintf(dmod,19,"%4d.%02d.%02d-%02d:%02d",tmdt.tm_year+1900,
                     tmdt.tm_mon+1,tmdt.tm_mday,tmdt.tm_hour,tmdt.tm_min);

            wprintff(mLog,"  %s  %s  %s  %s \n",bmod,copy,dmod,Drec[dii].file);
         }

         dii++;                                                                  //  next disk and archive files
         bii++;
      }

      goto report_exit;
   }

   //  report versions and expired versions per file

   if (strmatch(menu, "archived versions"))
   {
      Report("diffs summary");
      if (Bnf < 1) goto report_exit;

      wprintxx(mLog,0,"\n  lover hiver expver  loage hiage   bytes   expired  filespec \n",1);

      for (ii = 0; ii < Bnf; ii++)
      {
         lover = Brec[ii].lover;
         hiver = Brec[ii].hiver;
         nexpv = Brec[ii].nexpv;
         if (! lover) continue;

         strcpy(bfile,BJdirk);
         strcat(bfile,Brec[ii].file);
         loage = hiage = -1;
         mb1 = mb2 = 0.0;

         for (vers = lover; vers <= hiver; vers++)                               //  loop file versions
         {
            setFileVersion(bfile,vers);
            err = lstat(bfile,&statb);                                           //  check file exists on archive
            if (err) continue;

            fage = (time(0)-statb.st_mtime)/24.0/3600.0;                         //  file age in days
            age = fage;                                                          //  remove fraction
            if (loage < 0) loage = hiage = age;
            if (age < loage) loage = age;
            if (age > hiage) hiage = age;

            mb1 += statb.st_size;                                                //  accumulate total bytes
            if (vers < lover + nexpv)
               mb2 += statb.st_size;                                             //  and expired version bytes
         }  

         wprintff(mLog," %5d %5d %5d   %5d %5d  %8s %8s  %s \n",
                 lover,hiver,nexpv,loage,hiage,formatKBMB(mb1,3),formatKBMB(mb2,3),Brec[ii].file);
      }

      goto report_exit;
   }

   //  report expired file versions (will be purged)

   if (strmatch(menu, "expired versions"))
   {
      Report("diffs summary");
      if (Bnf < 1) goto report_exit;

      wprintxx(mLog,0,"\n  expired old file versions (to purge from archive location) \n",1);
      wprintxx(mLog,0,"\n  vers   age    bytes   filespec \n",1);

      for (ii = 0; ii < Bnf; ii++)
      {
         lover = Brec[ii].lover;
         hiver = Brec[ii].hiver;
         nexpv = Brec[ii].nexpv;
         if (! nexpv) continue;

         strcpy(bfile,BJdirk);
         strcat(bfile,Brec[ii].file);
         mb1 = 0.0;

         for (vers = lover; vers < lover + nexpv; vers++)                        //  loop expired file versions
         {
            setFileVersion(bfile,vers);
            err = lstat(bfile,&statb);                                           //  check file exists on archive
            if (err) continue;
            fage = (time(0)-statb.st_mtime)/24.0/3600.0;                         //  age in days, size in MB
            age = fage;
            mb1 = statb.st_size;
            wprintff(mLog," %5d %5d %8s   %s \n",vers,age,formatKBMB(mb1,3),Brec[ii].file);
         }
      }

      goto report_exit;
   }

   //  list all files in backup file set

   if (strmatch(menu, "list disk files"))
   {
      wprintxx(mLog,0," List all files in backup file set: \n",1);

      dGetFiles();
      wprintff(mLog,"   %d files found \n",Dnf);

      for (ii = 0; ii < Dnf; ii++)
         wprintff(mLog," %s \n",Drec[ii].file);

      goto report_exit;
   }

   //  list all files on archive archive

   if (strmatch(menu, "list archive files"))
   {
      wprintxx(mLog,0," List all files at archive location \n",1);
      if (bGetFiles() < 0) goto report_exit;

      for (ii = 0; ii < Bnf; ii++)
      {
         if (Brec[ii].lover) wprintff(mLog," %s (+ vers %d-%d) \n",
                     Brec[ii].file, Brec[ii].lover, Brec[ii].hiver);
         else  wprintff(mLog," %s \n",Brec[ii].file);
      }

      goto report_exit;
   }

   //  search disk and archive files for match with wildcard search pattern

   if (strmatch(menu, "find files"))
   {
      wprintxx(mLog,0," Find files matching wildcard pattern \n",1);

      dGetFiles();
      bGetFiles();
      if (!(Dnf + Bnf)) goto report_exit;

      fspec1 = zdialog_text(mWin,"enter (wildcard) filespec:","/dir*/file* ");
      if (! fspec1) goto report_exit;
      strncpy0(fspec2,fspec1,199);
      zfree(fspec1);
      strTrim(fspec2);
      if (! *fspec2) goto report_exit;

      wprintff(mLog,"\n matching disk files: \n");

      for (ii = 0; ii < Dnf; ii++)
         if (MatchWild(fspec2,Drec[ii].file) == 0)
               wprintff(mLog," %s \n",Drec[ii].file);

      wprintff(mLog,"\n matching archive files: \n");

      for (ii = 0; ii < Bnf; ii++)
      {
         if (MatchWild(fspec2,Brec[ii].file) == 0) {
            if (Brec[ii].hiver) wprintff(mLog," %s (+ vers %d-%d) \n",
                        Brec[ii].file, Brec[ii].lover, Brec[ii].hiver);
            else  wprintff(mLog," %s \n",Brec[ii].file);
         }
      }

      goto report_exit;
   }

report_exit:
   wprintff(mLog," ready \n");
   return 0;
}


/********************************************************************************/

//  show differences between backup file and prior file versions

namespace diffs_names
{
   char     *ukfile = 0, *initfile = 0;
   char     *p1file = 0, *initp1file = 0;
   char     *p2file = 0, *initp2file = 0;
   int      comp1, comp2, comp12;
   int      xlines, tabwidth;
   int      byside, showall, columns;
}


//  menu function

int FileDiffs(cchar *)                                                           //  6.0
{
   using namespace diffs_names;

   int  diffs_dialog_event(zdialog *zd, cchar *event);

   zdialog  *zd;

   if (! Bnf) bGetFiles();                                                       //  get backup media files
   if (! Bnf) return 0;
   zmainloop();

/****
          __________________________________________________
         |             file differences                     |
         |                                                  |
         | [browse] select a ukopp include file             |                      ukbutt
         |   /directories.../filename.ext                   |                      ukfile
         | [browse] select 1st prior file version           |                      p1butt
         |   /directories.../filename.ext (12)              |                      p1file
         | [browse] select 2nd prior file version           |                      p2butt
         |   /directories.../filename.ext (11)              |                      p2file
         |                                                  |
         | (o) compare backup file with 1st prior version   |                      comp1
         | (o) compare backup file with 2nd prior version   |                      comp2
         | (o) compare 1st and 2nd prior file versions      |                      comp12
         |                                                  |
         | [  3 |-+]  include context lines                 |                      xlines
         | [  8 |-+]  tab width                             |                      tabwidth
         |                                                  |
         | [x] compare side by side  [x] show entire files  |                      byside  showall
         | [ 80 |-+]  columns per file side                 |                      columns
         |                                                  |
         |                                 [ GO ] [cancel]  |
         |__________________________________________________|
         
****/

   zd = zdialog_new("File Differences",mWin,"GO","Cancel",null);

   zdialog_add_widget(zd,"hbox","hbukbutt","dialog");
   zdialog_add_widget(zd,"button","ukbutt","hbukbutt","Browse","space=3");
   zdialog_add_widget(zd,"label","labfile","hbukbutt","select a ukopp include file","space=3");
   zdialog_add_widget(zd,"hbox","hbukfile","dialog");
   zdialog_add_widget(zd,"label","space","hbukfile",0,"space=10");
   zdialog_add_widget(zd,"frame","frukfile","hbukfile",0,"expand");
   zdialog_add_widget(zd,"text","ukfile","frukfile","(no file selected)");

   zdialog_add_widget(zd,"hbox","hbp1butt","dialog");
   zdialog_add_widget(zd,"button","p1butt","hbp1butt","Browse","space=3");
   zdialog_add_widget(zd,"label","labp1file","hbp1butt","select 1st prior file version","space=3");
   zdialog_add_widget(zd,"hbox","hbp1file","dialog");
   zdialog_add_widget(zd,"label","space","hbp1file",0,"space=10");
   zdialog_add_widget(zd,"frame","frp1file","hbp1file",0,"expand");
   zdialog_add_widget(zd,"text","p1file","frp1file","(no file selected)");

   zdialog_add_widget(zd,"hbox","hbp2butt","dialog");
   zdialog_add_widget(zd,"button","p2butt","hbp2butt","Browse","space=3");
   zdialog_add_widget(zd,"label","labp2file","hbp2butt","select 2nd prior file version","space=3");
   zdialog_add_widget(zd,"hbox","hbp2file","dialog");
   zdialog_add_widget(zd,"label","space","hbp2file",0,"space=10");
   zdialog_add_widget(zd,"frame","frp2file","hbp2file",0,"expand");
   zdialog_add_widget(zd,"text","p2file","frp2file","(no file selected)");

   zdialog_add_widget(zd,"hsep","hsep2","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbox21","dialog");
   zdialog_add_widget(zd,"check","comp1","hbox21","compare backup file with 1st prior version","space=3");
   zdialog_add_widget(zd,"hbox","hbox22","dialog");
   zdialog_add_widget(zd,"check","comp2","hbox22","compare backup file with 2nd prior version","space=3");
   zdialog_add_widget(zd,"hbox","hbox23","dialog");
   zdialog_add_widget(zd,"check","comp12","hbox23","compare 1st and 2nd prior file versions","space=3");

   zdialog_add_widget(zd,"hsep","hsep3","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbox31","dialog");
   zdialog_add_widget(zd,"spin","xlines","hbox31","0|99|1|3","space=3");
   zdialog_add_widget(zd,"label","labxlines","hbox31","include context lines","space=3");
   zdialog_add_widget(zd,"hbox","hbox33","dialog");
   zdialog_add_widget(zd,"spin","tabwidth","hbox33","0|20|1|8","space=3");
   zdialog_add_widget(zd,"label","labtabwidth","hbox33","tab width","space=3");

   zdialog_add_widget(zd,"hsep","hsep4","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbox41","dialog");
   zdialog_add_widget(zd,"check","byside","hbox41","compare side by side","space=3");
   zdialog_add_widget(zd,"check","showall","hbox41","show entire files","space=10");
   zdialog_add_widget(zd,"hbox","hbox43","dialog");
   zdialog_add_widget(zd,"spin","columns","hbox43","20|200|1|80","space=3");
   zdialog_add_widget(zd,"label","labcolumns","hbox43","columns per file side","space=3");
   
   zdialog_restore_inputs(zd);
   zdialog_run(zd,diffs_dialog_event,"parent");
   zdialog_wait(zd);
   wprintff(mLog," ready \n");
   return 1;
}


//  dialog event and completion function

int diffs_dialog_event(zdialog *zd, cchar *event)                                //  6.0
{
   using namespace diffs_names;

   char     *pp, temp[XFCC];
   char     *file1, *file2, *command;
   int      cc;
   
   if (strmatchN(event,"comp",4)) {                                              //  toggle radio buttons 
      zdialog_stuff(zd,"comp1",0);                                               //    comp1 comp2 comp12
      zdialog_stuff(zd,"comp2",0);
      zdialog_stuff(zd,"comp12",0);
      zdialog_stuff(zd,event,1);
      return 1;
   }
   
   if (strmatch(event,"ukbutt"))                                                 //  select file to compare
   {
      if (ukfile) initfile = ukfile;
      else if (BJnnx) initfile = BJfspec[0];
      else initfile = 0;
      pp = zgetfile("ukopp include file",MWIN,"file",initfile,1);
      if (pp) {
         if (ukfile) zfree(ukfile);
         ukfile = pp;
         zdialog_stuff(zd,"ukfile",ukfile);
      }
   }
   
   if (strmatch(event,"p1butt"))                                                 //  select prior version 1 to compare
   {
      if (ukfile && *BJdirk) {
         if (initp1file) zfree(initp1file);
         cc = strlen(ukfile) + strlen(BJdirk) + 2;
         initp1file = (char *) zmalloc(cc);
         strncatv(initp1file,cc,BJdirk,ukfile,null);
      }
      pp = zgetfile("1st prior file version",MWIN,"file",initp1file,1);
      if (pp) {
         if (p1file) zfree(p1file);
         p1file = pp;
         zdialog_stuff(zd,"p1file",pp);
      }
   }

   if (strmatch(event,"p2butt"))                                                 //  select prior version 2 to compare
   {
      if (ukfile && *BJdirk) {
         if (initp2file) zfree(initp2file);
         cc = strlen(ukfile) + strlen(BJdirk) + 2;
         initp2file = (char *) zmalloc(cc);
         strncatv(initp2file,cc,BJdirk,ukfile,null);
      }
      pp = zgetfile("2nd prior file version",MWIN,"file",initp2file,1);
      if (pp) {
         if (p2file) zfree(p2file);
         p2file = pp;
         zdialog_stuff(zd,"p2file",pp);
      }
   }

   zdialog_fetch(zd,"byside",byside);                                            //  show all only if side-by-side report
   if (! byside) zdialog_stuff(zd,"showall",0);

   if (! zd->zstat) return 1;                                                    //  wait for dialog completion

   if (zd->zstat != 1) {                                                         //  cancel or [x]
      zdialog_free(zd);
      return 1;
   }
   
   zd->zstat = 0;                                                                //  keep dialog active
   
   file1 = file2 = 0;                                                            //  get all dialog inputs

   if (ukfile) zfree(ukfile);                                                    //  ukopp include file
   ukfile = 0;
   zdialog_fetch(zd,"ukfile",temp,XFCC);
   if (*temp) ukfile = zstrdup(temp);

   if (p1file) zfree(p1file);                                                    //  prior version 1
   p1file = 0;
   zdialog_fetch(zd,"p1file",temp,XFCC);
   if (*temp) p1file = zstrdup(temp);

   if (p2file) zfree(p2file);                                                    //  prior version 2
   p2file = 0;
   zdialog_fetch(zd,"p2file",temp,XFCC);
   if (*temp) p2file = zstrdup(temp);

   zdialog_fetch(zd,"comp1",comp1);                                              //  compare option
   zdialog_fetch(zd,"comp2",comp2);
   zdialog_fetch(zd,"comp12",comp12);

   zdialog_fetch(zd,"xlines",xlines);                                            //  context lines
   zdialog_fetch(zd,"tabwidth",tabwidth);

   zdialog_fetch(zd,"byside",byside);                                            //  side-by-side options
   zdialog_fetch(zd,"showall",showall);
   zdialog_fetch(zd,"columns",columns);

   if (comp1) {
      file1 = p1file;
      file2 = ukfile;
      if (! file1 || ! file2) {
         zmessageACK(mWin,"supply backup file and 1st prior version");
         return 1;
      }
   }
   
   else if (comp2) {
      file1 = p2file;
      file2 = ukfile;
      if (! file1 || ! file2) {
         zmessageACK(mWin,"supply backup file and 2nd prior version");
         return 1;
      }
   }
   
   else if (comp12) {
      file1 = p1file;
      file2 = p2file;
      if (! file1 || ! file2) {
         zmessageACK(mWin,"supply 1st and 2nd prior versions");
         return 1;
      }
   }
   
   else {
      zmessageACK(mWin,"choose a compare option");
      return 1;
   }
   
   if (byside)                                                                   //  side-by-side report
   {
      pp = strrchr(file1,'/');                                                   //  output a header line
      strcpy(temp,pp+1);
      cc = strlen(temp);
      memset(temp+cc,' ',columns);
      pp = strrchr(file2,'/');
      strcpy(temp+columns+2,pp+1);
      shell_ack("echo \"%s\" > ukopp_temp.txt",temp);
   }
   
   cc = strlen(file1) + strlen(file2) + 200;                                     //  bugfix 6.0 
   command = (char *) zmalloc(cc);

   strcpy(command,"diff -t ");                                                   //  construct diff command

   if (tabwidth) {
      sprintf(temp,"--tabsize=%d ",tabwidth);
      strcat(command,temp);
   }

   if (byside)                                                                   //  side by side report
   {
      strcat(command,"-y ");
      sprintf(temp,"-W %d ",2*columns);
      strcat(command,temp);
      if (! showall) strcat(command,"--suppress-common-lines ");
   }
   else                                                                          //  conventional diff report
   {
      if (xlines) {
         sprintf(temp,"-U %d ",xlines);                                          //  context lines before/after changes
         strcat(command,temp);
      }
   }
   
   strncatv(command,cc,"\"",file1,"\""," ","\"",file2,"\""," >> ukopp_temp.txt; ",
                        "xdg-open ukopp_temp.txt; rm ukopp_temp.txt",null);
   shell_ack(command);
   zfree(command);

   return 1;
}


/********************************************************************************/

//  edit dialog for file restore

int  RJedit_fchooser(cchar *dirk);
zdialog *RJedit_fchooser_zd = 0;

int RJedit(cchar *menu)
{
   int RJedit_dialog_event(zdialog *zd, cchar *event);

   zdialog        *zd;

   wprintff(mLog,"\n Restore files from archive \n");

   if (bGetFiles() < 0) return 0;                                                //  get files in archive location
   wprintff(mLog,"   %d archive files found \n",Bnf);
   if (! Bnf) return 0;

/****
       _________________________________________________
      |      restore files from archive                 |
      |                                                 |
      | copy-from archive [__________________________]  |
      |  copy-to disk     [__________________________]  |
      |                                                 |
      |  __________ files to restore _________________  |
      | |                                             | |
      | |                                             | |
      | |                                             | |
      | |                                             | |
      | |                                             | |
      | |                                             | |
      | |_____________________________________________| |
      |                                                 |
      |                       [browse] [done] [cancel]  |
      |_________________________________________________|
      
****/

   zd = zdialog_new("restore files from archive",mWin,"browse","done","cancel",null);
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=10");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"homog");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"homog|space=5|expand");
   zdialog_add_widget(zd,"label","labfrom","vb1","copy-from archive");           //  copy-from archive   [_____________]
   zdialog_add_widget(zd,"label","labto","vb1","copy-to disk");                  //  copy-to disk        [_____________]
   zdialog_add_widget(zd,"entry","entfrom","vb2",RJfrom);
   zdialog_add_widget(zd,"entry","entto","vb2",RJto);
   zdialog_add_widget(zd,"hsep","hsep1","dialog");
   zdialog_add_widget(zd,"label","labf","dialog","files to restore");            //  files to restore
   zdialog_add_widget(zd,"frame","framef","dialog",0,"expand");                  //  scrolling edit window
   zdialog_add_widget(zd,"scrwin","scrf","framef");
   zdialog_add_widget(zd,"edit","editf","scrf");

   editwidget = zdialog_widget(zd,"editf");

   for (int ii = 0; ii < RJnnx; ii++)                                            //  get restore include/exclude recs,
   {                                                                             //   pack into file selection edit box
      if (RJrtype[ii] == 2)
         wprintff(editwidget,"include %s\n",RJfspec[ii]);
      if (RJrtype[ii] == 3)
         wprintff(editwidget,"exclude %s\n",RJfspec[ii]);
   }
   
   zdialog_resize(zd,500,400);
   zdialog_run(zd,RJedit_dialog_event,"20/20");                                  //  run dialog
   zdialog_wait(zd);
   return 0;
}


//  dialog completion function
//  get restore job data from dialog widgets and validate

int RJedit_dialog_event(zdialog *zd, cchar *event)
{
   DIR         *pdirk;
   char        *pp, *fspec, rdirk[300];
   int         ftf = 1, cc, rtype, nerrs = 0;
   int         zstat;
   const char  *errmess = 0;

   zstat = zd->zstat;
   if (! zstat) return 0;                                                        //  wait for dialog end

   zd->zstat = 0;                                                                //  this dialog continues

   if (RJedit_fchooser_zd)                                                       //  kill file chooser dialog if active
      zdialog_free(RJedit_fchooser_zd);

   if (zstat == 1)
   {                                                                             //  browse button, file-chooser dialog
      zdialog_fetch(zd,"entfrom",RJfrom,300);                                    //  copy-from location /dirk/xxx/.../
      strTrim(RJfrom);
      strcpy(rdirk,BJdirk);                                                      //  start at /media/xxx/dirk/xxx/
      strncat(rdirk,RJfrom,299);
      RJedit_fchooser(rdirk);                                                    //  do file chooser dialog
      return 0;
   }

   if (zstat != 1 && zstat != 2) {                                               //  cancel or destroy
      zdialog_free(zd);
      return 0;
   }

   RJreset();                                                                    //  edit done, reset job data

   zdialog_fetch(zd,"entfrom",RJfrom,300);                                       //  copy-from location /dirk/xxx/.../
   strTrim(RJfrom);

   strcpy(rdirk,BJdirk);                                                         //  validate copy-from location
   strncat(rdirk,RJfrom,299);                                                    //  /media/xxx/dirk/...
   pdirk = opendir(rdirk);
   if (! pdirk) {
      zmessageACK(mWin,"invalid copy-from location");
      nerrs++;
   }
   else closedir(pdirk);

   cc = strlen(RJfrom);                                                          //  insure '/' at end
   if (RJfrom[cc-1] != '/') strcat(RJfrom,"/");

   zdialog_fetch(zd,"entto",RJto,300);                                           //  copy-to location  /dirk/yyy/.../
   strTrim(RJto);

   pdirk = opendir(RJto);                                                        //  validate copy-to location
   if (! pdirk) {
      zmessageACK(mWin,"invalid copy-to location");
      nerrs++;
   }
   else closedir(pdirk);

   cc = strlen(RJto);                                                            //  insure '/' at end
   if (RJto[cc-1] != '/') strcat(RJto,"/");
   
   for (RJnnx = 0; RJnnx < maxnx; RJnnx++)                                       //  include/exclude recs from edit box
   {
      pp = wscanf(editwidget,ftf);                                               //  next record from edit widget
      if (! pp) break;
      wprintff(mLog," %s \n",pp);

      errmess = parseRXrec(pp,rtype,fspec);                                      //  validate include/exclude rec.
      if (errmess) {
         zmessageACK(mWin,"%s \n %s",pp,errmess);
         nerrs++;
      }

      RJrtype[RJnnx] = rtype;                                                    //  save job record
      RJfspec[RJnnx] = fspec;
   }

   if (RJnnx == maxnx) {
      zmessageACK(mWin,"max job records exceeded");
      nerrs++;
   }

   if (nerrs == 0) RJvalid = 1;

   if (RJvalid) {                                                                //  all OK
      rGetFiles();                                                               //  get files to restore
      zdialog_free(zd);                                                          //  destroy dialog
   }
   else zd->zstat = 0;                                                           //  errors, keep dialog open

   return 0;
}


//  file chooser dialog for restore job edit

int RJedit_fchooser(cchar *dirk) 
{
   int RJedit_fchooser_event(zdialog *zd, const char *event);

   RJedit_fchooser_zd = zdialog_new("Choose Files to Restore",mWin,"Done",null);
   zdialog *zd = RJedit_fchooser_zd;

   zdialog_add_widget(zd,"frame","fr1","dialog",0,"expand");
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","space","hb1",0,"expand");
   zdialog_add_widget(zd,"check","hidden","hb1","show hidden","space=5");
   zdialog_add_widget(zd,"button","incl","hb1","include","space=5");
   zdialog_add_widget(zd,"button","excl","hb1","exclude","space=5");

   fc_widget = gtk_file_chooser_widget_new(GTK_FILE_CHOOSER_ACTION_OPEN);
   GtkWidget *frame = zdialog_widget(zd,"fr1");
   gtk_container_add(GTK_CONTAINER(frame),fc_widget);

   gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(fc_widget),dirk);
   gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(fc_widget),1);

   zdialog_resize(zd,700,500);
   zdialog_run(zd,RJedit_fchooser_event,"50/50");
   zdialog_wait(zd);
   zdialog_free(zd);
   RJedit_fchooser_zd = 0;
   return 0;
}


int RJedit_fchooser_event(zdialog *zd, const char *event)
{
   GSList      *flist = 0;
   STATB       statb;
   char        *file1, *file2, rdirk[300];
   int         ii, rdcc, err;

   if (strmatch(event,"hidden")) {                                               //  show/hide hidden files
      zdialog_fetch(zd,"hidden",ii);
      gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(fc_widget),ii);
   }

   if (strmatch(event,"incl") || strmatch(event,"excl"))                         //  include or exclude
   {
      strcpy(rdirk,BJdirk);                                                      //  copy-from location
      strncat(rdirk,RJfrom,299);                                                 //  /media/xxx/dirk/...
      rdcc = strlen(rdirk);

      flist = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(fc_widget));

      for (ii = 0; ; ii++)                                                       //  process selected files
      {
         file1 = (char *) g_slist_nth_data(flist,ii);
         if (! file1) break;

         if (! strmatchN(rdirk,file1,rdcc)) {                                    //  check file in archive location
            zmessageACK(mWin,"not in copy-from directory: \n %s",file1);
            continue;
         }

         err = lstat(file1,&statb);
         if (err) {
            zmessageACK(mWin,"error: %s  file: \n %s",strerror(errno),file1);
            continue;
         }

         file2 = zstrdup(file1,4);                                               //  extra space for wildcard
         g_free(file1);

         if (S_ISDIR(statb.st_mode)) strcat(file2,"/*");                         //  if directory, append wildcard

         if (strmatch(event,"incl"))
            wprintff(editwidget,"include %s""\n",file2 + BJdcc);                 //  omit backup mount point
         if (strmatch(event,"excl"))
            wprintff(editwidget,"exclude %s""\n",file2 + BJdcc);
         zfree(file2);
      }

      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(fc_widget));
      g_slist_free(flist);
   }

   return 0;
}


//  list and validate archive files to be restored

int RJlist(cchar *menu)
{
   int       cc1, cc2, errs = 0;
   char     *file1, file2[XFCC];

   if (! RJvalid) wprintff(mLog," *** restore job has errors \n");
   if (! Rnf) goto rjlist_exit;

   wprintff(mLog,"\n copy %d files from archive: %s \n",Rnf, RJfrom);
   wprintff(mLog,"    to directory: %s \n",RJto);
   wprintff(mLog,"\n resulting files will be the following: \n");

   cc1 = strlen(RJfrom);                                                         //  from: /dirk/xxx/.../
   cc2 = strlen(RJto);                                                           //    to: /dirk/yyy/.../

   for (int ii = 0; ii < Rnf; ii++)
   {
      file1 = Rrec[ii].file;

      if (! strmatchN(file1,RJfrom,cc1)) {
         wprintff(mLog," *** not within copy-from: %s \n",file1);
         errs++;
         continue;
      }

      strcpy(file2,RJto);
      strcpy(file2+cc2,file1+cc1);
      wprintff(mLog," %s \n",file2);
   }

   if (errs) {
      wprintff(mLog," *** %d errors \n",errs);
      RJvalid = 0;
   }

rjlist_exit:
   wprintff(mLog," ready \n");
   return 0;
}


//  restore files based on data from restore dialog

int Restore(cchar *menu)
{
   int         ii, nn, ccf;
   char        dfile[XFCC];
   const char  *errmess = 0;

   if (! RJvalid || ! Rnf) {
      wprintff(mLog," *** restore job has errors \n");
      goto restore_exit;
   }

   nn = zmessageYN(mWin,"Restore %d files from: %s%s \n     to: %s \n"
                   "Proceed with file restore ?",Rnf,BJdirk,RJfrom,RJto);
   if (! nn) goto restore_exit;

   snprintf(dfile,XFCC-2,"\n""begin restore of %d files to: %s \n",Rnf,RJto);
   wprintxx(mLog,0,dfile,1);

   gdk_window_set_cursor(mLogwin,watchcursor);                                   //  busy cursor

   ccf = strlen(RJfrom);                                                         //  from: /media/xxx/filespec

   for (ii = 0; ii < Rnf; ii++)
   {
      strcpy(dfile,RJto);                                                        //  to: /destination/filespec
      strcat(dfile,Rrec[ii].file + ccf);
      wprintff(mLog," %s \n",dfile);
      errmess = copyFile(Rrec[ii].file,dfile,1,0);
      if (errmess) wprintff(mLog," *** %s \n",errmess);
      else Rrec[ii].finc = 1;
      if (checkKillPause()) goto restore_exit;
   }

   synch_poop("restore");                                                        //  synch owner and permissions data

restore_exit:
   wprintff(mLog," ready \n");
   killFlag = 0;
   gdk_window_set_cursor(mLogwin,0);                                             //  normal cursor
   return 0;
}


/********************************************************************************/

//  display help/about or help/user guide

int helpFunc(cchar *menu)
{
   if (strmatch(menu,"about")) {
      wprintff(mLog," %s \n",ukopp_title);
      wprintff(mLog," free software: %s \n",ukopp_license);
   }

   if (strmatch(menu,"user guide")) showz_userguide();
   return 0;
}


//  convert seconds to "hh:mm:ss" format

void format_seconds(double secs, char *text)                                     //  v.5.4
{
   int      hours, minutes, seconds;
   
   seconds = secs;
   hours = seconds / 3600;
   seconds = seconds - 3600 * hours;
   minutes = seconds / 60;
   seconds = seconds - 60 * minutes;
   sprintf(text,"%02d:%02d:%02d",hours,minutes,seconds);
   return;
}


//  Mount archive device. Return 1 if success, else 0.
//  menu caller: menu arg is present
//  internal caller: menu arg is 0

int mount(cchar *menu)                                                           //  more error checking
{
   int         err;
   char        work[300];
   const char  *errmess;
   STATB       statb;

   bFilesReset();                                                                //  clear file data at archive location
   DevPoop();                                                                    //  refresh device data
   
   err = stat(BJdirk,&statb);                                                    //  create archive directory if req.   5.7
   if (err) {
      wprintff(mLog," create directory: %s \n",BJdirk);
      err = mkdir(BJdirk,0700);
      if (err) {
         wprintff(mLog," *** failed: %s \n",strerror(errno));
         return 0;
      }
   }

   err = stat(BJdirk,&statb);                                                    //  check directory exists             5.7
   if (err || ! S_ISDIR(statb.st_mode)) {
      wprintff(mLog," *** not a directory: %s \n",BJdirk);
      return 0;
   }

   errmess = validate_archive();                                                 //  validate archive location
   if (errmess) {
      wprintff(mLog," %s \n",errmess);
      return 0;
   }

   if (! *BJdev) return 1;                                                       //  directory only, no mount req. 
   
   if (devMounted) {
      wprintff(mLog," already mounted \n");
      return 1;
   }
   else {
      snprintf(work,300,"mount -noatime %s %s",BJdev,BJdirk);                    //  mount archive device
      err = do_shell("mount",work);
      if (err) {
         wprintff(mLog," *** mount failed: %s \n",strerror(errno));
         return 0;
      }
      devMounted = 1;
      ukoppMounted = 1;                                                          //  remember mounted by me
   }

   DevPoop();                                                                    //  refresh device data
   return 1;
}


//  unmount archive device

int unmount(cchar *menu)                                                         //  revised v.5.4
{
   char     work[100];

   bFilesReset();                                                                //  no files at archive location

   snprintf(work,100,"umount %s",BJdev);
   do_shell("umount",work);

   devMounted = ukoppMounted = 0;
   DevPoop();                                                                    //  refresh device data
   return 0;
}


//  save logging window as text file

int saveLog(cchar *menu)
{
   wfilesave(mLog,MWIN);
   return 0;
}


//  backup helper function
//  write date and time to temp file

int writeDT()
{
   time_t      dt1;
   char        *dt2;
   FILE        *fid;
   int         cc;

   time(&dt1);
   dt2 = ctime(&dt1);                                                            //  get string date-time
   cc = strlen(dt2);
   if (cc && (dt2[cc-1] == '\n')) dt2[cc-1] = 0;                                 //  save without trailing \n

   fid = fopen(TFdatetime,"w");
   if (! fid) zappcrash("cannot open scratch file %s",TFdatetime);

   fprintf(fid,"%s \n",dt2);
   fclose(fid);
   return 0;
}


/********************************************************************************/

//  synchronize owner and permissions data using poopfile at archive location
//   - for files copied archive >> disk, set owner and permissions from poopfile
//   - refresh poopfile data from disk files
//  mode is "backup" "restore" or "synch"

int synch_poop(const char *mode)
{
   int         ii, jj, err, nn, uid, gid, perms;
   int         cc, ccf, cct;
   char        pfile[XFCC], rfile[XFCC];
   char        dirk[XFCC], pdirk[XFCC];
   char        tfile[XFCC];
   char        *pp, poopfile[100];
   const char  *errmess = 0;
   FILE        *fid;
   STATB       statb;

   if (strmatch(mode,"synch"))                                                   //  set poop for updated disk files
   {
      strcpy(poopfile,BJdirk);
      strcat(poopfile,BD_POOPFILE);
      fid = fopen(poopfile,"r");                                                 //  open poopfile
      if (! fid) {
         wprintff(mLog," *** no owner/permissions file: %s \n",poopfile);
         return 0;
      }

      ii = 0;

      while (true)                                                               //  read poopfile records
      {
         nn = fscanf(fid,"%d:%d %o %[^\n]",&uid,&gid,&perms,pfile);              //  uid, gid, perms, file or directory
         if (nn == EOF) break;
         if (nn != 4) continue;

         cc = strlen(pfile);

         while (ii < Bnf)                                                        //  match poopfile file or directory
         {                                                                       //    to archive files copied to disk
            nn = strncmp(Brec[ii].file,pfile,cc);                                //  (logic assumes ascii sort)
            if (nn >= 0) break;
            ii++;
         }

         if (ii == Bnf) break;                                                   //  EOL
         if (nn > 0) continue;                                                   //  file not in archive file list
         if (Brec[ii].finc == 0) continue;                                       //  file not copied to disk

         wprintff(mLog," set owner/perms %d:%d %04o %s \n",uid,gid,perms,pfile);
         err = chown(pfile,uid,gid);
         if (err) wprintff(mLog," *** error: %s \n",strerror(errno));
         err = chmod(pfile,perms);
         if (err) wprintff(mLog," *** error: %s \n",strerror(errno));
      }

      fclose(fid);
      return 0;
   }

   if (strmatch(mode,"restore"))                                                 //  set owner/perms for restored disk files
   {                                                                             //  overhauled to handle versions      5.4
      if (Rnf == 0) {
         wprintff(mLog," *** no files were restored \n");
         return 0;
      }

      strcpy(poopfile,BJdirk);
      strcat(poopfile,BD_POOPFILE);                                              //  open poopfile with owner/perms data
      fid = fopen(poopfile,"r");
      if (! fid) {
         wprintff(mLog," *** no owner/permissions file: %s \n",poopfile);
         return 0;
      }
      
      ccf = strlen(RJfrom);
      cct = strlen(RJto);
      
      *pfile = *rfile = 0;                                                       //  initz. first files to compare 
      ii = -1;
      
      while (true)
      {
         restore_compare:                                                        //  compare poopfile to restored file
 
         nn = strcmp(pfile,rfile);
         
         if (nn < 0 || *pfile == 0)                                              //  poopfile < restored file
         {
            while (true)                                                         //  get next poopfile
            {
               nn = fscanf(fid,"%d:%d %o %[^\n]",&uid,&gid,&perms,pfile);
               if (nn == EOF) goto restore_return;
               if (nn != 4) continue;
               err = stat(pfile,&statb);
               if (err) continue;
               if (S_ISREG(statb.st_mode)) goto restore_compare;                 //  regular file
               if (S_ISDIR(statb.st_mode)) {                                     //  directory
                  if (! strstr(rfile,pfile)) continue;                           //  must be parent of restored file
                  strcpy(tfile,RJto);
                  strcpy(tfile + cct, pfile + ccf);
                  wprintff(mLog," set owner/perms %d:%d %04o %s \n",uid,gid,perms,tfile);
                  err = chown(tfile,uid,gid);
                  if (err) wprintff(mLog," *** error: %s \n",strerror(errno));
                  err = chmod(tfile,perms);
                  if (err) wprintff(mLog," *** error: %s \n",strerror(errno));
               }
            }
         }
         
         if (nn > 0 || *rfile == 0)                                              //  poopfile > restored file
         {
            ii++;                                                                //  get next restored file
            while (ii < Rnf && Rrec[ii].finc == 0) ii++;                         //  skip to next restored file
            if (ii == Rnf) goto restore_return;                                  //  EOL
            strcpy(rfile,Rrec[ii].file);                                         //  restored filespec
            pp = rfile + strlen(rfile);
            if (strmatchN(pp-1,VSEP2,1)) {                                       //  filespec ends with ")"
               for (jj = 4; jj < 8; jj++)                                        //  look back for " (" 
                  if (strmatchN(pp-jj,VSEP1,2)) break;                           //  found " (nn)"
               if (jj < 8) *(pp-jj) = 0;                                         //  remove " (nn)" from filespec
            }
            goto restore_compare;
         }

         strcpy(tfile,RJto);                                                     //  poopfile = restore file
         strcpy(tfile + cct, rfile + ccf);
         wprintff(mLog," set owner/perms %d:%d %04o %s \n",uid,gid,perms,tfile);
         err = chown(tfile,uid,gid);
         if (err) wprintff(mLog," *** error: %s \n",strerror(errno));
         err = chmod(tfile,perms);
         if (err) wprintff(mLog," *** error: %s \n",strerror(errno));

         *rfile = ' ';                                                           //  force get next restored file
      }

      restore_return:
 
      fclose(fid);
      return 0;
   }

   if (strmatch(mode,"backup") || strmatch(mode,"synch"))                        //  make new poop file from disk files
   {
      fid = fopen(TFpoopfile,"w");
      if (! fid) zappcrash("cannot open temp file %s",TFpoopfile);

      *pdirk = 0;                                                                //  no prior directory

      for (ii = 0; ii < Dnf; ii++)
      {
         strcpy(dirk,Drec[ii].file);                                             //  next file on disk
         pp = dirk;

         while (true)                                                            //  set directory owner & permissions
         {
            pp = strchr(pp+1,'/');                                               //  next (last) directory level
            if (! pp) break;
            cc = pp - dirk + 1;                                                  //  cc incl. '/'
            if (strncmp(dirk,pdirk,cc) == 0) continue;                           //  matches prior, skip

            *pp = 0;                                                             //  terminate this directory level

            err = lstat(dirk,&statb);                                            //  get owner and permissions
            if (err) {
               wprintff(mLog," *** error: %s  file: %s \n",strerror(errno),dirk);
               break;
            }

            statb.st_mode = statb.st_mode & 0777;

            fprintf(fid,"%4d:%4d %3o %s/\n",                                     //  output uid:gid perms directory/
                    statb.st_uid, statb.st_gid, statb.st_mode, dirk);

            *pp = '/';                                                           //  restore '/'
         }

         strcpy(pdirk,dirk);                                                     //  prior = this directory

         strcpy(rfile,Drec[ii].file);                                            //  disk file, again

         err = lstat(rfile,&statb);                                              //  get owner and permissions
         if (err) {
            wprintff(mLog," *** error: %s  file: %s \n",strerror(errno),rfile);
            continue;
         }

         statb.st_mode = statb.st_mode & 0777;

         fprintf(fid,"%4d:%4d %3o %s\n",                                         //  output uid:gid perms file
                 statb.st_uid, statb.st_gid, statb.st_mode, rfile);
      }

      fclose(fid);

      errmess = copyFile(TFpoopfile,BD_POOPFILE,2,BJcopyop);                     //  copy file owner/permissions file
      if (errmess) wprintff(mLog," *** poopfile error: %s \n",errmess);
      return 0;
   }

   return 0;
}


/********************************************************************************/

//  get all disk files specified by include/exclude records
//  save in Drec[] array

int dGetFiles()
{
   const char  *fsp, *psep2;
   char        *fspec, *pp, *psep1;
   int         ftf, wstat, cc, err, dups;
   int         rtype, ii, jj, st, nfiles;
   int         fcc, vers;
   double      nbytes;
   STATB       statb;

   dFilesReset();
   wprintx(mLog,0,"\n""generating backup file set ...\n",1);

   for (ii = 0; ii < BJnnx; ii++)                                                //  process include/exclude recs
   {
      BJfiles[ii] = 0;                                                           //  initz. include/exclude rec stats
      BJbytes[ii] = 0.0;

      if (! BJfspec[ii]) continue;

      if (*BJfspec[ii] == '"') {
         fspec = zstrdup(BJfspec[ii]+1,4);                                       //  unquote quoted filespec
         pp = strrchr(fspec,'"');
         if (pp) *pp = 0;
      }
      else fspec = zstrdup(BJfspec[ii],4);                                       //  space for /*

      rtype = BJrtype[ii];

      if (rtype == 2 || rtype == 3)                                              //  include or exclude filespec
      {
         err = stat(fspec,&statb);
         if (! err && S_ISDIR(statb.st_mode)) {                                  //  if directory, append /*
            cc = strlen(fspec) - 1;
            if (fspec[cc] != '/') cc++;
            strcpy(fspec+cc,"/*");
         }
      }

      if (rtype == 2)                                                            //  include filespec
      {
         ftf = 1;

         while (1)
         {
            fsp = SearchWild(fspec,ftf);                                         //  find matching files
            if (! fsp) break;

            Drec[Dnf].file = zstrdup(fsp);

            err = lstat(fsp,&statb);                                             //  check accessibility
            if (! err) {
               Drec[Dnf].err = 0;
               if (! S_ISREG(statb.st_mode) &&                                   //  reg. files + symlinks only
                   ! S_ISLNK(statb.st_mode)) continue;
            }
            else Drec[Dnf].err = errno;                                          //  save file error status

            fcc = strlen(fsp);
            psep1 = (char *) strstr(fsp+fcc-10,VSEP1);                           //  look for file version
            if (psep1) {
               vers = 0;
               st = convSI(psep1+2,vers,&psep2);                                 //  if format not valid, take
               if (st < 2) vers = 1;                                             //    as non-versioned file
               if (! strmatch(psep2,VSEP2)) vers = 0;
               if (*(psep2+1)) vers = 0;                                         //  VSEP2 must be at end
               if (vers) {
                  wprintff(mLog," *** omit versioned file: %s \n",fsp);
                  continue;
               }
            }

            Drec[Dnf].jindx = ii;                                                //  save pointer to include record
            Drec[Dnf].size = statb.st_size;                                      //  save file size
            Drec[Dnf].mtime = statb.st_mtime                                     //  save last mod time
                            + statb.st_mtim.tv_nsec * nano;                      //    (nanosec resolution)
            if (err) Drec[Dnf].size = Drec[Dnf].mtime = 0;                       //  inaccessible file
            Drec[Dnf].finc = 0;                                                  //  not copied yet
            Drec[Dnf].bindx = -1;                                                //  no link to archive record yet

            BJfiles[ii]++;                                                       //  count included files and bytes
            BJbytes[ii] += Drec[Dnf].size;

            if (++Dnf == maxfs) {
               wprintff(mLog," *** max files exceeded \n");
               goto errret;
            }
         }
      }

      if (rtype == 3)                                                            //  exclude filespec
      {
         for (jj = 0; jj < Dnf; jj++)                                            //  check all included files (SO FAR)
         {
            if (! Drec[jj].file) continue;
            wstat = MatchWild(fspec,Drec[jj].file);
            if (wstat != 0) continue;
            BJfiles[ii]--;                                                       //  un-count excluded file and bytes
            BJbytes[ii] -= Drec[jj].size;
            zfree(Drec[jj].file);                                                //  clear file data entry
            Drec[jj].file = 0;
            Drec[jj].err = 0;
         }
      }

      zfree(fspec);
   }                                                                             //  end of include/exclude recs

   for (ii = 0; ii < Dnf; ii++)                                                  //  list and remove error files
   {                                                                             //  (after excluded files removed)
      if (Drec[ii].err) {
         wprintff(mLog," *** %s  omit: %s \n",strerror(Drec[ii].err),Drec[ii].file);
         jj = Drec[ii].jindx;
         BJfiles[jj]--;                                                          //  un-count file and bytes
         BJbytes[jj] -= Drec[ii].size;
         zfree(Drec[ii].file);
         Drec[ii].file = 0;
      }
   }

   ii = jj = 0;                                                                  //  repack file arrays after deletions
   while (ii < Dnf)
   {
      if (Drec[ii].file == 0) ii++;
      else {
         if (ii > jj) {
            if (Drec[jj].file) zfree(Drec[jj].file);
            Drec[jj] = Drec[ii];
            Drec[ii].file = 0;
         }
         ii++;
         jj++;
      }
   }

   Dnf = jj;                                                                     //  final file count in backup set

   Dbytes = 0.0;
   for (ii = 0; ii < Dnf; ii++) Dbytes += Drec[ii].size;                         //  compute total bytes from files

   nfiles = 0;
   nbytes = 0.0;

   for (ii = 0; ii < BJnnx; ii++)                                                //  compute total files and bytes
   {                                                                             //    from include/exclude recs
      nfiles += BJfiles[ii];
      nbytes += BJbytes[ii];
   }

   wprintff(mLog," disk files: %d  %s \n",nfiles,formatKBMB(nbytes,3));

   if ((nfiles != Dnf) || (Dbytes != nbytes)) {                                  //  must match
      wprintff(mLog," *** bug: nfiles: %d  Dnf: %d \n",nfiles,Dnf);
      wprintff(mLog,"          nbytes: %.0f  Dbytes: %.0f \n",nbytes,Dbytes);
      goto errret;
   }

   SortFileList((char *) Drec,sizeof(dfrec),Dnf,'A');                            //  sort Drec[Dnf] by Drec[].file

   for (ii = dups = 0; ii < Dnf-1; ii++)                                         //  look for duplicate files
      if (strmatch(Drec[ii].file,Drec[ii+1].file)) {
         wprintff(mLog," *** duplicate file: %s \n",Drec[ii].file);
         dups++;
      }

   if (dups) goto errret;
   
   wprintff(mLog,"\n");
   return 0;

errret:
   BJvalid = 0;
   dFilesReset();
   wprintff(mLog,"\n");
   return 0;
}


/********************************************************************************/

//  get existing files at archive location, save in Brec[] array
//  return -1 if error, else count of archive files

int bGetFiles()
{
   int         gcc, fcc, err, vers, vfound, jj, kk;
   int         bb, bbp, rtype, noret = 0;
   int         lover, hiver, retNV, retND;
   char        command[300], *pp, *psep1;
   char        bfile[XFCC], *bfile2;
   double      fage;
   const char  *psep2;
   FILE        *fid;
   STATB       statb;

   bFilesReset();                                                                //  reset archive file list
   if (! mount(0)) return 0;                                                     //  validate and mount archive

   wprintx(mLog,0,"\n""find all files at archive location ...\n",1);

   sprintf(command,"find %s -type f -or -type l >%s",BJdirk,TFbakfiles);         //  backup filespecs to temp file
   err = do_shell("find",command);
   if (err) return -1;

   //  read filespecs into memory and use memory sort instead of linux sort utility
   //  (apparently cannot do a straight ascii sort, even with LC_ALL=C)

   gcc = strlen(BD_UKOPPDIRK);                                                   //  directory for ukopp special files

   fid = fopen(TFbakfiles,"r");                                                  //  read file list
   if (! fid) zappcrash("cannot open scratch file %s",TFbakfiles);

   for (bb = 0; bb < maxfs; )                                                    //  loop all files at archive location
   {
      pp = fgets_trim(bfile,XFCC-1,fid);                                         //  next file
      if (! pp) break;                                                           //  eof

      bfile2 = bfile + BJdcc;                                                    //  remove backup mount point
      if (strmatchN(bfile2,BD_UKOPPDIRK,gcc)) continue;

      fcc = strlen(bfile2);
      if (fcc > XFCC-BJdcc-10) {                                                 //  cannot handle files near limit
         wprintff(mLog," *** filespec too big, omit: %s...",bfile2);
         wprintff(mLog,"\n");
         continue;
      }

      err = lstat(bfile,&statb);                                                 //  check accessibility
      if (err) {
         wprintff(mLog," *** %s, omit: %s",strerror(errno),bfile2);
         wprintff(mLog,"\n");
         continue;
      }
      else  if (! S_ISREG(statb.st_mode) &&                                      //  reg. files and symlinks only
                ! S_ISLNK(statb.st_mode)) continue;

      //  build memory record for file data

      Brec[bb].file = zstrdup(bfile2);                                           //  filespec
      Brec[bb].err = 0;
      Brec[bb].size = statb.st_size;                                             //  file size
      Brec[bb].mtime = statb.st_mtime                                            //  last mod time
                      + statb.st_mtim.tv_nsec * nano;
      Brec[bb].lover = Brec[bb].hiver = Brec[bb].nexpv = 0;                      //  no versions yet
      Brec[bb].finc = 0;                                                         //  no backup yet
      bb++;
   }

   fclose (fid);

   Bnf = bb;
   wprintff(mLog," %6d archive files \n",Bnf);

   if (Bnf == maxfs) {
      wprintff(mLog," *** max files exceeded \n");
      bFilesReset();
      return -1;
   }

   SortFileList((char *) Brec,sizeof(bfrec),Bnf,'A');                            //  sort Brec[Bnf] by Brec[].file

   for (bb = 0, bbp = -1; bb < Bnf; bb++)                                        //  loop all files
   {
      bfile2 = Brec[bb].file;
      fcc = strlen(bfile2);

      vers = 0;
      psep1 = strstr(bfile2+fcc-10,VSEP1);                                       //  look for file version
      if (psep1) {
         err = convSI(psep1+2,vers,1,9999,&psep2);                               //  if format not valid,
         if (err > 1) vers = 0;                                                  //    assume a current file (vers 0)
         if (! strmatch(psep2,VSEP2)) vers = 0;
         if (*(psep2+1)) vers = 0;                                               //  VSEP2 must be at end
         if (vers) *psep1 = 0;                                                   //  remove version from file name
      }

      if (! vers)                                                                //  a current file, not prior version
      {
         bbp++;                                                                  //  add new file record
         Brec[bbp] = Brec[bb];                                                   //  copy all data
      }

      if (vers)                                                                  //  a prior version, 1-9999
      {
         if (bbp > -1 && strmatch(Brec[bbp].file,bfile2)) {                      //  look back for match with prior file
            if (Brec[bbp].lover == 0) Brec[bbp].lover = vers;                    //  set first version found
            if (vers < Brec[bbp].lover) Brec[bbp].lover = vers;                  //  (10) sorts before (9)
            if (vers > Brec[bbp].hiver) Brec[bbp].hiver = vers;                  //  track lowest and highest vers. found
            zfree(bfile2);                                                       //  free duplicate filespec
         }
         else  {                                                                 //  version present, but no curr. file
            bbp++;                                                               //  add new file record
            Brec[bbp] = Brec[bb];                                                //  copy all data
            Brec[bbp].err = -1;                                                  //  mark file (vers 0) not present
            Brec[bbp].size = Brec[bbp].mtime = 0;
            Brec[bbp].lover = Brec[bbp].hiver = vers;                            //  track prior versions present
         }
      }
   }

   Bnf = bbp + 1;

   for (bb = 0; bb < Bnf; bb++)                                                  //  loop all files at archive location
   {
      strcpy(bfile,BJdirk);
      strcat(bfile,Brec[bb].file);
      bfile2 = bfile + BJdcc;

      if (BJnnx > 0) {
         kk = -1;
         for (jj = 0; jj < BJnnx; jj++) {                                        //  find matching backup include rec.
            rtype = BJrtype[jj];
            if (rtype != 2) continue;
            if (MatchWild(BJfspec[jj],bfile2) == 0) kk = jj;                     //  remember last match                5.3
         }
         if (kk < 0) {                                                           //  this file not in backup set
            Brec[bb].retNV = Brec[bb].retND = 0;                                 //  no retention specs
            noret++;
         }
         else {
            jj = kk;                                                             //  last matching include rec.         5.3
            Brec[bb].retNV = BJretNV[jj];                                        //  get corresp. retention specs
            Brec[bb].retND = BJretND[jj];
         }
      }

      if (Brec[bb].err == 0) {
         Cfiles++;                                                               //  count curr. version files
         Cbytes += Brec[bb].size;                                                //    and total bytes
      }

      if (Brec[bb].lover == 0) continue;                                         //  no versions present

      lover = Brec[bb].lover;                                                    //  version range found
      hiver = Brec[bb].hiver;
      retNV = Brec[bb].retNV;                                                    //  retention versions
      retND = Brec[bb].retND;                                                    //  retention days

      if (! retND) retND = -1;                                                   //  zero days retention, defeat test
      vfound = 0;                                                                //  versions found

      for (vers = hiver; vers >= lover; vers--)                                  //  loop file version, high to low
      {
         setFileVersion(bfile,vers);
         err = lstat(bfile,&statb);                                              //  check file exists on archive
         if (err) continue;

         vfound++;                                                               //  this file, versions found
         Vfiles++;                                                               //  total versioned files and bytes
         Vbytes += statb.st_size;

         fage = (time(0)-statb.st_mtime)/24.0/3600.0;                            //  file version age in days
         if (vfound <= retNV && fage <= retND) continue;                         //  this version is to be retained
         Brec[bb].nexpv++;                                                       //  count expired versions
         Pfiles++;                                                               //  total expired files and bytes
         Pbytes += statb.st_size;                                                //  (to be purged)
      }
   }

   wprintff(mLog," %6d files not in backup set (unknown retention) \n",noret);
   wprintff(mLog," %6d (%s) curr. file versions \n",Cfiles,formatKBMB(Cbytes,3));
   wprintff(mLog," %6d (%s) prior file versions \n",Vfiles,formatKBMB(Vbytes,3));
   wprintff(mLog," %6d (%s) expired old file versions \n",Pfiles,formatKBMB(Pbytes,3));

   sprintf(command,"df -h %s",BJdirk);                                           //  BJdirk not mountdirk bugfix        5.4
   do_shell("df",command);

   return Bnf;
}


/********************************************************************************/

//  get all restore files specified by include/exclude records
//  save in Rrec[] array

int rGetFiles()
{
   int      ii, jj, cc, err;
   int      rtype, wstat, ninc, nexc;
   char     *fspec, rfile[XFCC];
   STATB    statb;

   if (! RJvalid) return 0;
   rFilesReset();                                                                //  clear restore files
   if (bGetFiles() < 1) return 0;                                                //  get archive files

   wprintff(mLog,"\n generating restore file set \n");

   for (ii = 0; ii < RJnnx; ii++)                                                //  process include/exclude recs
   {
      rtype = RJrtype[ii];
      fspec = RJfspec[ii];

      if (rtype == 2)                                                            //  include filespec
      {
         wprintff(mLog," include %s \n",fspec);
         
         snprintf(rfile,XFCC,"%s/%s",BJdirk,fspec);
         err = stat(rfile,&statb);
         if (! err) {
            Rrec[Rnf].file = zstrdup(fspec);                                     //  add matching files
            Rrec[Rnf].finc = 0;
            Rnf++; ninc++;
            if (Rnf == maxfs) {
               wprintff(mLog," *** max files exceeded \n");
               rFilesReset();
               return 0;
            }
            wprintff(mLog,"  1 file added \n");
            continue;
         }

         for (ninc = jj = 0; jj < Bnf; jj++)                                     //  screen all files in archive loc.
         {
            wstat = MatchWild(fspec,Brec[jj].file);
            if (wstat != 0) continue;
            if (Brec[jj].err) continue;
            Rrec[Rnf].file = zstrdup(Brec[jj].file);                             //  add matching files
            Rrec[Rnf].finc = 0;
            Rnf++; ninc++;
            if (Rnf == maxfs) {
               wprintff(mLog," *** max files exceeded \n");
               rFilesReset();
               return 0;
            }
         }

         wprintff(mLog,"  %d files added \n",ninc);
      }

      if (rtype == 3)                                                            //  exclude filespec
      {
         wprintff(mLog," exclude %s \n",fspec);

         for (nexc = jj = 0; jj < Rnf; jj++)                                     //  check all included files (SO FAR)
         {
            if (! Rrec[jj].file) continue;

            wstat = MatchWild(fspec,Rrec[jj].file);
            if (wstat != 0) continue;
            zfree(Rrec[jj].file);                                                //  remove matching files
            Rrec[jj].file = 0;
            nexc++;
         }

         wprintff(mLog,"  %d files removed \n",nexc);
      }
   }

   ii = jj = 0;                                                                  //  repack after deletions
   while (ii < Rnf)
   {
      if (Rrec[ii].file == 0) ii++;
      else
      {
         if (ii > jj)
         {
            if (Rrec[jj].file) zfree(Rrec[jj].file);
            Rrec[jj].file = Rrec[ii].file;
            Rrec[ii].file = 0;
         }
         ii++;
         jj++;
      }
   }

   Rnf = jj;
   wprintff(mLog," total file count: %d \n",Rnf);

   cc = strlen(RJfrom);                                                          //  copy from: /dirk/.../

   for (ii = 0; ii < Rnf; ii++)                                                  //  get selected archive files to restore
   {
      if (! strmatchN(Rrec[ii].file,RJfrom,cc)) {
         wprintff(mLog," *** not within copy-from: %s \n",Rrec[ii].file);
         RJvalid = 0;                                                            //  mark restore job invalid
         continue;
      }
   }

   SortFileList((char *) Rrec,sizeof(rfrec),Rnf,'A');                            //  sort Rrec[Rnf] by Rrec[].file
   return 0;
}


/********************************************************************************/

//  helper function for backups and reports
//
//  compare disk and archive files, set disp in Drec[] and Brec[] arrays:
//       n  new         on disk, not on archive
//       d  deleted     on archive, not on disk
//       m  modified    on both, but not equal
//       u  unchanged   on both, and equal
//       v  versions    on archive, only prev. versions are present

int setFileDisps()
{
   int            dii, bii, comp;
   char           disp;
   double         diff;

   dii = bii = 0;
   nnew = nmod = nunc = ndel = nexpv = comp = 0;
   Mbytes = 0.0;                                                                 //  total bytes, new and modified files

   while ((dii < Dnf) || (bii < Bnf))                                            //  scan disk and archive files parallel
   {
      if ((dii < Dnf) && (bii == Bnf)) comp = -1;
      else if ((dii == Dnf) && (bii < Bnf)) comp = +1;
      else comp = strcmp(Drec[dii].file, Brec[bii].file);

      if (comp < 0) {                                                            //  unmatched disk file
         Drec[dii].disp = 'n';                                                   //  new
         nnew++;                                                                 //  count new files
         Mbytes += Drec[dii].size;                                               //  accumulate Mbytes
         Drec[dii].bindx = -1;                                                   //  no matching archive file
         dii++;
      }

      else if (comp > 0) {                                                       //  unmatched archive file
         if (Brec[bii].err == 0) {                                               //  if current version is present,
            Brec[bii].disp = 'd';                                                //    file was deleted from disk
            ndel++;                                                              //  count deleted files
         }
         else Brec[bii].disp = 'v';                                              //  only old versions on archive
         nexpv += Brec[bii].nexpv;                                               //  accumulate expired versions        5.8
         bii++;
      }

      else if (comp == 0) {                                                      //  file present on disk and archive
         Drec[dii].bindx = bii;                                                  //  link disk to archive record
         if (Brec[bii].err == 0) {
            diff = Drec[dii].mtime - Brec[bii].mtime;                            //  check if equal mod times
            if (fabs(diff) > MODTIMETOLR) disp = 'm';
            else disp = 'u';                                                     //  yes, assume unchanged
            Drec[dii].disp = Brec[bii].disp = disp;
            if (disp == 'u') nunc++;                                             //  count unchanged files
            if (disp == 'm') nmod++;                                             //  count modified files
            if (disp == 'm') Mbytes += Drec[dii].size;                           //    and accumulate Mbytes
         }
         else {
            Brec[bii].disp = 'v';                                                //  only old versions on archive
            Drec[dii].disp = 'n';                                                //  disk file is logically new
            nnew++;                                                              //  count new files
            Mbytes += Drec[dii].size;                                            //  accumulate Mbytes
         }
         nexpv += Brec[bii].nexpv;                                               //  accumulate expired versions        5.8
         dii++;
         bii++;
      }
   }

   Mfiles = nnew + nmod + ndel;
   return 0;
}


/********************************************************************************/

//  Sort file list in memory (disk files, archive files, restore files).
//  Sort ascii sequence, or sort subdirectories in a directory before files.

int SortFileList(char *recs, int RL, int NR, char sort)
{
   HeapSortUcomp fcompA, fcompD;                                                 //  filespec compare funcs
   if (sort == 'A') HeapSort(recs,RL,NR,fcompA);                                 //  ascii compare
   if (sort == 'D') HeapSort(recs,RL,NR,fcompD);                                 //  special compare (directories first)
   return 0;
}

int fcompA(cchar *rec1, cchar *rec2)                                             //  ascii comparison
{                                                                                //  current file (no version) sorts first
   dfrec  *r1 = (dfrec *) rec1;
   dfrec  *r2 = (dfrec *) rec2;
   return strcmp(r1->file,r2->file);
}

int fcompD(cchar *rec1, cchar *rec2)                                             //  special compare filenames
{                                                                                //  subdirectories in a directory compare
   dfrec  *r1 = (dfrec *) rec1;                                                  //    less than files in the directory
   dfrec  *r2 = (dfrec *) rec2;
   return filecomp(r1->file,r2->file);
}

int filecomp(cchar *file1, cchar *file2)                                         //  special compare filenames
{                                                                                //  subdirectories compare before files
   cchar       *pp1, *pp10, *pp2, *pp20;
   cchar       slash = '/';
   int         cc1, cc2, comp;

   pp1 = file1;                                                                  //  first directory level or file
   pp2 = file2;

   while (true)
   {
      pp10 = strchr(pp1,slash);                                                  //  find next slash
      pp20 = strchr(pp2,slash);

      if (pp10 && pp20) {                                                        //  both are directories
         cc1 = pp10 - pp1;
         cc2 = pp20 - pp2;
         if (cc1 < cc2) comp = strncmp(pp1,pp2,cc1);                             //  compare the directories
         else comp = strncmp(pp1,pp2,cc2);
         if (comp) return comp;
         else if (cc1 != cc2) return (cc1 - cc2);
         pp1 = pp10 + 1;                                                         //  equal, check next level
         pp2 = pp20 + 1;
         continue;
      }

      if (pp10 && ! pp20) return -1;                                             //  only one is a directory,
      if (pp20 && ! pp10) return 1;                                              //    the directory is first

      comp = strcmp(pp1,pp2);                                                    //  both are files, compare
      return comp;
   }
}


/********************************************************************************/

//  reset all backup job data and free allocated memory

int BJreset()
{
   for (int ii = 0; ii < BJnnx; ii++)
      if (BJfspec[ii]) zfree(BJfspec[ii]);

   BJnnx = BJvalid = 0;
   BJvmode = 0;
   dFilesReset();                                                                //  reset dependent disk file data
   return 0;
}


//  reset all restore job data and free allocated memory

int RJreset()
{
   for (int ii = 0; ii < RJnnx; ii++)
      if (RJfspec[ii]) zfree(RJfspec[ii]);

   RJvalid = RJnnx = 0;
   rFilesReset();                                                                //  reset dependent disk file data
   return 0;
}


//  reset all file data and free allocated memory

int dFilesReset()
{                                                                                //  disk files data
   for (int ii = 0; ii < Dnf; ii++)
   {
      zfree(Drec[ii].file);
      Drec[ii].file = 0;
   }

   Dnf = 0;
   Dbytes = Mbytes = 0.0;
   return 0;
}

int bFilesReset()
{                                                                                //  archive files data
   for (int ii = 0; ii < Bnf; ii++)
   {
      zfree(Brec[ii].file);
      Brec[ii].file = 0;
   }

   Bbytes = Bnf = 0;
   Cbytes = Cfiles = 0;
   Mbytes = Mfiles = 0;
   Vbytes = Vfiles = 0;
   Pbytes = Pfiles = 0;
   return 0;
}

int rFilesReset()
{                                                                                //  restore files data
   for (int ii = 0; ii < Rnf; ii++)
   {
      zfree(Rrec[ii].file);
      Rrec[ii].file = 0;
   }

   Rnf = 0;
   return 0;
}


/********************************************************************************/

//  Helper function to copy a file between disk and archive location.
//  mpf = 1/2: prepend mount point to source/destination file
//  copf: copy owner/permissions from source to destination file
//        copy owner/permissions for created destination directories

cchar * copyFile(cchar *sfile, cchar *dfile, int mpf, int copf)
{
   char        file1[XFCC], file2[XFCC];
   int         fid1, fid2, err, rcc, dlevs;
   char        *pp1, *pp2, buff[BIOCC];
   const char  *errmess = 0;
   STATB       statb1, statb2;
   struct timeval    ftimes[2];

   *file1 = *file2 = 0;
   if (mpf == 1) strcpy(file1,BJdirk);                                           //  prepend mount point if req.
   strcat(file1,sfile);
   if (mpf == 2) strcpy(file2,BJdirk);
   strcat(file2,dfile);

   pp2 = file2;
   dlevs = 0;

   while (true) {
      pp2 = strchr(pp2+1,'/');                                                   //  create missing directory levels
      if (! pp2) break;                                                          //  (check and create from top down)
      *pp2 = 0;
      err = stat(file2,&statb2);
      if (err) {
         err = mkdir(file2,0731);
         if (err) return strerror(errno);
         dlevs++;
      }
      *pp2 = '/';
   }

   if (copf) {                                                                   //  copy owner/permissions option      5.3
      while (dlevs) {
         pp1 = (char *) strrchr(file1,'/');                                      //  for created output directories,
         if (! pp1) break;                                                       //   copy owner and permissions from
         pp2 = (char *) strrchr(file2,'/');                                      //    corresponding input directory
         if (! pp2) break;                                                       //     (measured from bottom up)
         *pp1 = *pp2 = 0;                                                        //  (possibly upper levels not set)
         err = stat(file1,&statb1);
         if (err) return strerror(errno);
         chmod(file2,statb1.st_mode);
         err = chown(file2,statb1.st_uid,statb1.st_gid);
         if (err) wprintff(mLog,"error: %s \n",wstrerror(err));
         dlevs--;
      }
   }

   *file1 = *file2 = 0;
   if (mpf == 1) strcpy(file1,BJdirk);                                           //  refresh filespecs
   strcat(file1,sfile);
   if (mpf == 2) strcpy(file2,BJdirk);
   strcat(file2,dfile);

   err = lstat(file1,&statb1);                                                   //  get input file attributes
   if (err) return strerror(errno);

   if (S_ISLNK(statb1.st_mode)) {                                                //  input file is symlink
      rcc = readlink(file1,buff,XFCC);
      if (rcc < 0 || rcc > XFCC-2) return strerror(errno);
      buff[rcc] = 0;
      err = symlink(buff,file2);                                                 //  create output symlink
      if (err) return strerror(errno);
      ftimes[0].tv_sec = statb1.st_atime;                                        //  get input file access time
      ftimes[0].tv_usec = statb1.st_atim.tv_nsec / 1000;                         //    in microsecs.
      ftimes[1].tv_sec = statb1.st_mtime;
      ftimes[1].tv_usec = statb1.st_mtim.tv_nsec / 1000;
      lutimes(file2,ftimes);                                                     //  set output file access time
      return 0;
   }

   fid1 = open(file1,O_RDONLY+O_NOATIME+O_LARGEFILE);                            //  open input file
   if (fid1 == -1) return strerror(errno);

   fid2 = open(file2,O_WRONLY+O_CREAT+O_TRUNC+O_LARGEFILE,0700);                 //  open output file
   if (fid2 == -1) {
      errmess = strerror(errno);
      close(fid1);
      return errmess;
   }

   while (true)
   {
      rcc = read(fid1,buff,BIOCC);                                               //  read huge blocks
      if (rcc == 0) break;
      if (rcc == -1) {
         errmess = strerror(errno);
         close(fid1);
         close(fid2);
         return errmess;
      }

      rcc = write(fid2,buff,rcc);                                                //  write blocks
      if (rcc == -1) {
         errmess = strerror(errno);
         close(fid1);
         close(fid2);
         return errmess;
      }

      if (checkKillPause()) break;
   }

   close(fid1);                                                                  //  close input file
   err = fsync(fid2);                                                            //  flush output file
   if (err) return strerror(errno);
   err = close(fid2);                                                            //  close output file
   if (err) return strerror(errno);

   err = lstat(file1,&statb1);                                                   //  get input file attributes
   if (err) return strerror(errno);

   if (copf) {                                                                   //  copy owner/permissions option      5.3
      chmod(file2,statb1.st_mode);                                               //  copy owner and permissions
      err = chown(file2,statb1.st_uid,statb1.st_gid);                            //    from input to output file
      if (err) wprintff(mLog,"error: %s \n",wstrerror(err));
   }

   ftimes[0].tv_sec = statb1.st_atime;                                           //  get input file access time
   ftimes[0].tv_usec = statb1.st_atim.tv_nsec / 1000;                            //    in microsecs.
   ftimes[1].tv_sec = statb1.st_mtime;
   ftimes[1].tv_usec = statb1.st_mtim.tv_nsec / 1000;
   utimes(file2,ftimes);                                                         //  set output file access time

   return 0;
}


/********************************************************************************/

//  Verify helper function
//  Verify that file on archive is readable, return its length.
//  Optionally compare archive file to disk file, byte for byte.
//  returns error message or null if OK.

cchar * checkFile(cchar *dfile, int compf, double &tcc)
{
   int         vfid = 0, dfid = 0;
   int         err, vcc, dcc, cmperr = 0;
   char        vfile[XFCC], *vbuff = 0, *dbuff = 0;
   const char  *errmess = 0;
   double      dtime, vtime;
   int         open_flagsV = O_RDONLY+O_NOATIME+O_LARGEFILE;                     //  O_DIRECT fails with kernel 3.16    5.1
   int         open_flagsD = O_RDONLY+O_NOATIME+O_LARGEFILE;
   STATB       statb;

   tcc = 0.0;

   strcpy(vfile,BJdirk);                                                         //  prepend mount point
   strcat(vfile,dfile);

   lstat(vfile,&statb);                                                          //  if symlink, check readable
   if (S_ISLNK(statb.st_mode)) {
      vbuff = (char *) malloc(XFCC);
      vcc = readlink(vfile,vbuff,XFCC);
      if (vcc == -1) errmess = strerror(errno);
      goto cleanup;
   }

   if (compf) goto comparefiles;

   vfid = open(vfile,open_flagsV);                                               //  open for read, large blocks, direct I/O
   if (vfid == -1) goto checkerr;

   err = posix_memalign((void**) &vbuff,4096,BIOCC);                             //  use aligned buffer
   if (err) zappcrash("memory allocation failure");

   while (1)
   {
      vcc = read(vfid,vbuff,BIOCC);
      if (vcc <= 0) break;
      tcc += vcc;                                                                //  accumulate length
      if (checkKillPause()) break;
   }

   if (vcc < 0) errmess = strerror(errno);
   goto cleanup;

comparefiles:

   vfid = open(vfile,open_flagsV);                                               //  archive file to verify, direct I/O
   if (vfid == -1) goto checkerr;

   dfid = open(dfile,open_flagsD);                                               //  source file to compare, cached I/O
   if (dfid == -1) goto checkerr;

   err = posix_memalign((void**) &vbuff,512,BIOCC);                              //  use aligned buffers
   if (err) zappcrash("memory allocation failure");
   err = posix_memalign((void**) &dbuff,512,BIOCC);
   if (err) zappcrash("memory allocation failure");

   while (1)
   {
      vcc = read(vfid,vbuff,BIOCC);                                              //  read two files
      if (vcc == -1) { errmess = strerror(errno); goto cleanup; }

      dcc = read(dfid,dbuff,BIOCC);
      if (dcc == -1) { errmess = strerror(errno); goto cleanup; }

      if (vcc != dcc) cmperr++;                                                  //  compare buffers
      if (memcmp(vbuff,dbuff,vcc)) cmperr++;

      tcc += vcc;                                                                //  accumulate length
      if (vcc == 0) break;
      if (dcc == 0) break;

      if (checkKillPause()) break;
   }

   if (vcc != dcc) cmperr++;

   if (cmperr) {                                                                 //  compare error
      lstat(dfile,&statb);
      dtime = statb.st_mtime + statb.st_mtim.tv_nsec * nano;                     //  file modified since snapshot?
      lstat(vfile,&statb);
      vtime = statb.st_mtime + statb.st_mtim.tv_nsec * nano;
      if (fabs(dtime-vtime) < MODTIMETOLR) errmess = "compare error";            //  no, a real compare error
   }

cleanup:
   if (vfid) close(vfid);
   if (dfid) close(dfid);
   if (vbuff) free(vbuff);
   if (dbuff) free(dbuff);
   return errmess;

checkerr:
   errmess = strerror(errno);
   if (errno == EINVAL || errno == ENOTSUP) 
      zmessageACK(mWin,"large block direct I/O not allowed \n %s",errmess);
   if (errno == EPERM) 
      zmessageACK(mWin,"permission denied \n %s",errmess);

   goto cleanup;
}


/********************************************************************************/

//  modify filespec to have a specified version
//  0 = no version = current version, +N = previous version
//  returns cc of resulting filespec
//  warning: filespec must have space for version numbers

int setFileVersion(char *filespec, int vers)
{
   int         fcc, overs, err;
   char        *psep1;
   const char  *psep2;

   fcc = strlen(filespec);
   psep1 = strstr(filespec+fcc-10,VSEP1);                                        //  look for file version 
   if (psep1) {
      overs = 0;
      err = convSI(psep1+2,overs,&psep2);                                        //  if format not valid, take
      if (err < 2) overs = 1;                                                    //    as non-versioned file
      if (! strmatch(psep2,VSEP2)) overs = 0;
      if (*(psep2+1)) overs = 0;                                                 //  VSEP2 must be at end
      if (overs) *psep1 = 0;
      fcc = psep1 - filespec;
   }

   if (vers == 0) return fcc;

   if (! psep1) psep1 = filespec + fcc;
   strcpy(psep1,VSEP1);
   sprintf(psep1+2,"%d",vers);
   strcat(psep1+2,VSEP2);

   return fcc + strlen(psep1);
}


//  rename a archive file to assign the next version number
//  update the passed backup file data record, bakrec
//  returns error message or null if all OK

const char * setnextVersion(bfrec &bakrec)
{
   char     fspec1[XFCC], fspec2[XFCC];
   int      vers, err;

   strcpy(fspec1,BJdirk);
   strcat(fspec1,bakrec.file);
   strcpy(fspec2,fspec1);

   vers = bakrec.hiver + 1;
   setFileVersion(fspec2,vers);

   err = rename(fspec1,fspec2);
   if (err) return strerror(errno);

   bakrec.hiver = vers;
   if (! bakrec.lover) bakrec.lover = vers;
   return null;
}


//  purge expired file versions in archive location
//  bakrec: backup file data record to use and update
//  returns error message or null if all OK
//  fkeep: if true, keep last version unless disk file was deleted 

const char * purgeVersions(bfrec &bakrec, int fkeep)
{
   int         lover, hiver, loretver, vers;
   int         err, vfound, vpurged;
   int         retNV, retND;
   double      fage;
   char        fspec[XFCC];
   const char  *mess = null;
   STATB       statb;

   strcpy(fspec,BJdirk);                                                         //  prepend archive location
   strcat(fspec,bakrec.file);

   retNV = bakrec.retNV;
   retND = bakrec.retND;
   lover = bakrec.lover;
   hiver = bakrec.hiver;
   if (! hiver) return 0;                                                        //  no versions present

   if (! retND) retND = -1;                                                      //  zero days retention, defeat test

   if (bakrec.disp == 'd') fkeep = 0;                                            //  file no longer in backup job
   if (bakrec.disp == 'v') fkeep = 0;                                            //    or disk file deleted

   loretver = lover;                                                             //  lowest retained version
   vfound = 0;                                                                   //  actual versions found
   vpurged = 0;                                                                  //  versions purged

   for (vers = hiver; vers >= lover; vers--)                                     //  loop file versions, high to low
   {
      setFileVersion(fspec,vers);
      err = lstat(fspec,&statb);                                                 //  check file exists on archive
      if (err) continue;
      vfound++;                                                                  //  count versions found

      fage = (time(0)-statb.st_mtime)/24.0/3600.0;                               //  file age in days

      if (vfound <= retNV && fage <= retND) {                                    //  retain this version
         loretver = vers;                                                        //  remember lowest retained version
         continue;
      }

      if ((vers == hiver) && fkeep) {                                            //  retain last version
         loretver = vers;
         continue;
      }

      mess = deleteFile(fspec);                                                  //  purge this version
      if (mess) break;
      vpurged++;
   }

   bakrec.nexpv = fkeep;                                                         //  set 0 or 1 expired versions
   bakrec.lover = loretver;                                                      //  set new low version
   if (vpurged == vfound)
      bakrec.lover = bakrec.hiver = 0;                                           //  no versions remaining
   return mess;
}


//  helper function to delete a file from archive location.
//  delete parent directories if they are now empty.

cchar * deleteFile(cchar *file)
{
   int      err;
   char     dfile[XFCC], *pp;

   strcpy(dfile,file);

   err = remove(dfile);
   if (err) return strerror(errno);

   while ((pp = (char *) strrchr(dfile,'/')))                                    //  delete empty directory
   {
      *pp = 0;
      err = rmdir(dfile);
      if (! err) continue;                                                       //  and parents ...
      if (errno == ENOTEMPTY) return 0;
      if (errno == EBUSY) return 0;                                              //  see lkml.org/lkml/2013/8/28/654 
      return strerror(errno);
   }

   return 0;
}


//  do shell command (subprocess) and echo outputs to log window
//  returns command status: 0 = OK, +N = error

int do_shell(cchar *pname, cchar *command)
{
   char     buff[500], *crec;
   int      err, contx = 0;

   snprintf(buff,499,"\n""shell: %s \n",command);
   wprintx(mLog,0,buff,1);

   while ((crec = command_output(contx,command)))                                //  bug fix: remove colon
   {
      wprintff(mLog," %s: %s \n",pname,crec);
      zsleep(0.1);                                                               //  throttle output a little
   }

   err = command_status(contx);
   if (err) wprintff(mLog," %s status: %s \n", pname, strerror(err));
   else wprintff(mLog," OK \n");
   return err;
}


//  supply unused zdialog callback function

void KBevent(GdkEventKey *event)
{ return; }




